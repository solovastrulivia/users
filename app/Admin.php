<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;  //-in spate la User extinde notificable
//use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
//use App\Notifications\UserResetPasswordNotification;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'age'//, 'company_id'
    ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}



// namespace App;

// use Illuminate\Database\Eloquent\Model;

// class Admin extends Model
// {
//     /**
//      * The attributes that are mass assignable.
//      *
//      * @var array
//      */
//     protected $fillable = [
//         'company_id', 'country_id', 'name', 'email', 'password', 'age'
//     ];
//     /**
//      * The attributes that should be hidden for arrays.
//      *
//      * @var array
//      */
//     protected $hidden = [
//         'password', 'remember_token',
//     ];


// }
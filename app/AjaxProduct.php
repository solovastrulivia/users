<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AjaxProduct extends Model
{
    protected $table ='ajax_products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'detail'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table ='companies';
    protected $primaryKey = 'id';
    protected $fillable = ['company_name', 'profile', 'cui', 'adress'];
    public $timestamps=false;

public function products()
	{
		return $this->hasMany('\App\Product', 'company_id', 'id');
	}
}

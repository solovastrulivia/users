<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table ='countries';

    protected $guarded =[];

    public function posts() {
    	return $this->hasManyThrough('App\Post', 'App\User', 'country_id'); //, 'user_id' al4-lea //users is intermediate table where is country_id
    }
}

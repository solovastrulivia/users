<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded =[];
 
    public function Note(){
        return $this->hasMany('App\Note', 'id', 'course_id'); //invese in many to many
    }
}

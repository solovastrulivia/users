<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Page;
use App\User;
use DB;
use Session;
use Lang;

class AjaxController extends Controller
{
	public function allEx(){
    	return view('ajax.ajax_all');
    } 

    public function ajaxEx1(){
    	return view('ajax.ajax_ex1');
    }

    public function ajaxEx1Post(Request $request){
    	$msg = "This is a simple message.";
      	return response()->json(array('msg'=> $msg), 200);
    }

    public function ajaxEx2(){
    	return view('ajax.ajax_ex2');
    }

    public function ajaxEx2Post(Request $request){
    	//dd($request->all());

    	$input = $request->all();

        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }

    public function searchPages(){
    	return view('ajax.search_pages');
    }

    public function searchPagesAction(Request $request){
    	//dd($request->all());

    	if($request->ajax())
    	{
    		$query = $request->get('query');
    		if($query != '')
    		{
    			$data = DB::table('pages')->where('title', 'like', '%'.$query.'%')
    									->orWhere('author', 'like', '%'.$query.'%')
    									->orderBy('id', 'desc')->get();
    		}
    		else
    		{
    			$data = DB::table('pages')->orderBy('id', 'desc')->get();
    			//dd($data);
    		}
    		$total_row = $data->count();
    		$output='';
    		if($total_row > 0)
    		{
    			foreach ($data as $row) {
    				$output .= '
						<tr>
							<td>'.$row->title.'</td>
							<td>'.$row->author.'</td>
							<td>'.$row->chapters.'</td>
							<td>'.$row->no_pages_day.'</td>
						</tr>
    				';
    			}
    		}
    		else
    		{
    			$output = '
    			<tr>
					<td class="text-center" colspan="4"> No data found. </td>
    			</tr>';	
    		}
    		$data = [
    			'table_data' => $output,
    			'total_data' => $total_row
    		];
    	}

        echo json_encode($data);
    }

    public function showUtilizare(){
    	return view('ajax.show_utilizare');
    }

    public function showUtilizareAction(Request $request){
    	//dd($request->all());
    	if($request->ajax())
    	{
    		$data = DB::table('utilizare')->orderBy('Nr', 'desc')->get();
    		$total_row = $data->count();
    		$output='';
    		if($total_row > 0)
    		{
    			foreach ($data as $row) {
    				$output .= '
						<tr>
							<td>'.$row->Denumire.'</td>
							<td>'.$row->Denumire_stiintifica.'</td>
							<td>'.$row->Indicatii.'</td>
							<td>'.$row->Mod_utilizare.'</td>
						</tr>
    				';
    			}
    		}
    		else
    		{
    			$output = '
    			<tr>
					<td class="text-center" colspan="4"> No data found. </td>
    			</tr>';	
    		}
    		$data = [
    			'table_data' => $output,
    			'total_data' => $total_row
    		];
    	}

        echo json_encode($data);
    }


    public function showModalPage(){
        //$pages = Page::all();
        //dd($pages);

        return view('ajax.modal_page');
    }

    public function index(){
        $pages = Page::all();

        return view('ajax.index', compact('pages'));
    }

    public function showModal(Request $request){
        $page = Page::where('id', $request->id)->first();

        return view('ajax.modal', compact('page'));
    }

    public function createModal(){
         $user_name = User::pluck('name', 'id');
         //dd($user_name);

        return view('ajax.create_modal', compact('user_name'));
    }

    public function storeModal(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'title' => 'required',
            'author' => 'required',
            'chapters' => 'required',
            'no_pages_day' => 'required|integer'
        ]);

        if ($validator->fails()) {
            Session::flash('alert-message', $validator->messages()->first() );
            Session::flash('alert-class', 'danger');
            return redirect()->back()->withInput();
        }

        $page = Page::create([
            'user_id' => $request->user_id,
            'title' => $request->title,
            'author' => $request->author,
            'chapters' => $request->chapters,
            'no_pages_day' => $request->no_pages_day
        ]);

        if($page){
            Session::flash('alert-message', Lang::get('app.successfully_created_record'));
            Session::flash('alert-class', 'success');
        } else {
            Session::flash('alert-message', Lang::get('app.error_creating_record') );
            Session::flash('alert-class', 'danger');
        }
        
        return redirect( route('index') );
    }

    public function editModal(Request $request){
        $page = Page::where('id', $request->id)->first();

        return view('ajax.edit_modal', compact('page'));
    }

    public function updateModal(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'chapters' => 'required',
            'no_pages_day' => 'required|integer'
        ]);

        if ($validator->fails()) {
            Session::flash('alert-message', $validator->messages()->first() );
            Session::flash('alert-class', 'danger');
            return redirect()->back()->withInput();
        }

        // save data
        $book = Page::find($id);
        if($book) {
            $book->title = $request->title;
            $book->author = $request->author;
            $book->chapters = $request->chapters;
            $book->no_pages_day = $request->no_pages_day;
        }
             if($book->save() ){
                Session::flash('alert-message', Lang::get('app.successfully_created_record') );
                Session::flash('alert-class', 'success');
            }else{
                Session::flash('alert-message', Lang::get('app.error_creating_record') );
                Session::flash('alert-class', 'danger');
            }
        
        return redirect( route('index') );
    }

    public function destroy($id)
    {
        $book = Page::find($id);
        
        if($book)
            $book->delete();
        
        return redirect(route('index'));
    }

}

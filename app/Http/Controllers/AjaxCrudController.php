<?php

namespace App\Http\Controllers;

use App\AjaxCrud;
use Illuminate\Http\Request;
use DataTables;

class AjaxCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
   
        if ($request->ajax()) {
            $data = AjaxCrud::latest()->get();
            dd($data);
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('ajax_crud.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AjaxCrud  $ajaxCrud
     * @return \Illuminate\Http\Response
     */
    public function show(AjaxCrud $ajaxCrud)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AjaxCrud  $ajaxCrud
     * @return \Illuminate\Http\Response
     */
    public function edit(AjaxCrud $ajaxCrud)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AjaxCrud  $ajaxCrud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AjaxCrud $ajaxCrud)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AjaxCrud  $ajaxCrud
     * @return \Illuminate\Http\Response
     */
    public function destroy(AjaxCrud $ajaxCrud)
    {
        //
    }
}

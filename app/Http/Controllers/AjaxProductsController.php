<?php

namespace App\Http\Controllers;

use App\AjaxProduct;
use Illuminate\Http\Request;
use DataTables;

class AjaxProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = AjaxProduct::all();

        if ($request->ajax()) {
            $data = AjaxProduct::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';
   
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('ajax-products.ajaxProducts',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        AjaxProduct::updateOrCreate(['id' => $request->product_id],
                ['name' => $request->name, 'detail' => $request->detail]);        
   
        return response()->json(['success'=>'Product saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AjaxProduct  $ajaxProduct
     * @return \Illuminate\Http\Response
     */
    public function show(AjaxProduct $ajaxProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AjaxProduct  $ajaxProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = AjaxProduct::find($id);
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AjaxProduct  $ajaxProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AjaxProduct $ajaxProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AjaxProduct  $ajaxProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AjaxProduct::find($id)->delete();
     
        return response()->json(['success'=>'Product deleted successfully.']);
    }
}

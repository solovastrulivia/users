<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class BlacklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('blacklist.index');
    }

    public function dataTable(Request $request)
    {
        //dd($request->all());
        $q = $request->search['value'];
        $dir = $request->order[0]['dir'];
        $orderBy = $request->columns[$request->order[0]['column']]['data'];

        $totalRecords = User::all()->count();
        $queryData = User::when($q, function($que, $q){
                            return $que->where(function($que) use ($q){
                                return $que->where('name', 'LIKE', '%'.$q.'%')
                                ->orWhere('email', 'like', '%'.$q.'%')
                                ->orWhere('phone', 'like', '%'.$q.'%');
                            });
                    });

        $totalRecordwithFilter = $queryData->count();
        $list = $queryData->orderBy( $orderBy, $dir)->skip($request->start)->take($request->length)->get();

        $blacklist = [];
        foreach($list as $user){
            $blacklist[] = [
                'name' => $user->name,
                'email' => $user->email,
                'age' => $user->age,
                'created_at' => $user->created_at ? date('Y m d', strtotime($user->created_at)) : 'Old DB',
                'options' => '
                            <td class="text-right">
                                
                            </td>'
                            //<a href="' . route('blacklist.edit', $user->id) . '" class="btn btn-sm btn-default btn-rounded"><i class="fa fa-edit"></i><span class="btn-small-text">&nbsp;' .  __('app.edit') . '</span></a>
            ];
        }
        
        $response = [
            "recordsTotal" => $totalRecords,
            "recordsFiltered" => $totalRecordwithFilter,
            "data" => $blacklist
        ];
       
        return response()->json($response);
    }

    public function showDates()
    {
        return view('blacklist.dates');
    }

    public function dataTable2(Request $request)
    {
        //dd($request->all());
        $q = $request->search['value'];
        $dir = $request->order[0]['dir'];
        $orderBy = $request->columns[$request->order[0]['column']]['data'];

        $totalRecords = User::all()->count();
        $queryData = User::when($q, function($que, $q){
                            return $que->where(function($que) use ($q){
                                return $que->where('name', 'LIKE', '%'.$q.'%')
                                ->orWhere('email', 'like', '%'.$q.'%')
                                ->orWhere('phone', 'like', '%'.$q.'%');
                            });
                    });

        $totalRecordwithFilter = $queryData->count();
        $list = $queryData->orderBy( $orderBy, $dir)->skip($request->start)->take($request->length)->get();

        $blacklist = [];
        foreach($list as $user){
            $blacklist[] = [
                'name' => $user->name,
                'email' => $user->email,
                'age' => $user->age
            ];
        }
        
        $response = [
            "recordsTotal" => $totalRecords,
            "recordsFiltered" => $totalRecordwithFilter,
            "data" => $blacklist
        ];
       
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        //dd($books);

        return view('books.index', compact('books'));
    }

    public function storeBook(Request $request)
    {
        //dd($request->all());

        // Book::create(request()->validate([
        //     'name' => 'required',
        //     'quantity' => 'required',
        //     'price' => 'required'   //de reverificat
        // ]));
        $count = count($request->name);
        //dd($count);
        foreach($request->name as $k => $name){
            $amount = (float)$request->quantity[$k] * $request->price[$k];
            $amount  = number_format($amount, 2, '.', ' ');

            Book::create([
                'name' => $request->name[$k],
                'quantity' => $request->quantity[$k],
                'price' => $request->price[$k],
                'amount' => $amount
            ]);
        }

        return redirect( route('books.index') );

    }

    // public function updatePrice(Request $request)
    // {
    //     dd($request->all());

    //     // Book::create(request()->validate([
    //     //     'name' => 'required',
    //     //     'quantity' => 'required',
    //     //     'price' => 'required'   //de reverificat
    //     // ]));
    //     $amount  = $request->quantity * $request->price;

    //     Book::create([
    //         'name' => $request->name,
    //         'quantity' => $request->quantity,
    //         'price' => $request->price,
    //         'amount' => $amount
    //     ]);

    //     //dd('salvat');
    //     return redirect( route('books.index') );

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}

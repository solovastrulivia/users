<?php

namespace App\Http\Controllers;

use App\Company;
//use App\Product;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$companies = Company::all();
        //dd($companies);

        //$companies = Company::with('products')->get();

        // $companies = Company::with('products')->whereHas(
        //     'products', function($query) {
        //         return $query->where('products_no', 3)
        //         ->orWhere('product_name', 'poezii');
        //     })->select('company_name','profile','cui')
        //     ->get();

            //$companies =Company::where(['company_name'=>'CARTURESTI IASI SRL'])->select('company_name','profile','cui')->first();

            // $companies = Company::with('products.characteristic')->whereHas(
            //     'products', function($query) {
            //         return $query->where('products_no', 3);
            //     })->get();

            //$companies = Company::where('company_name','like','%d%')->get();

            $companies = Company::with('products.characteristic')
            ->whereHas(
                'products', function($query) {
                    return $query->where('products_no', '>', 1);
                })
            ->whereHas(
                'products.characteristic', function($query) {
                    return $query->where('quality', 'excelent');
                })
            // ->orWhere('company_name','like','%d%')
            ->get();

        //dd($companies);
        //return view('companies.companies', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}

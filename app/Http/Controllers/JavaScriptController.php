<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JavaScriptController extends Controller
{
    public function allEx(){
    	return view('javascript.js_all');
    } 

    public function showProduct(){
    	return view('javascript.product');
    } 

    public function calc(){
    	return view('javascript.calc');
    } 

    public function calculator(){
    	return view('javascript.calculator');
    } 
}

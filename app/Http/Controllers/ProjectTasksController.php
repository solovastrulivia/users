<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Project;

class ProjectTasksController extends Controller
{
	public function store(Project $project)
	{
		//$project->tasks()->create(request());

		$attributes = request()->validate(['description'=>'required']);
		//dd($attributes);

		$project->addTask($attributes); //request('description') <= $attributes
		// Task::create([
		// 	'project_id'=>$project->id,
		// 	'description'=> request('description')
		// ]);
		return back();
	}

    public function update(Task $task)
    {
    	//dd($task);
    	//$task->complete(request()->has('completed'));
    	$method = request()->has('completed')?"complete":"incomplete";
    	$task->$method();

    	/*
    	if(request()->has('completed')){
    		$task->complete();
    	}else{
    		$task->incomplete();
    	}
    	*/
    	//request()->has('completed')?$task->complete():$task->incomplete();
    	//$method = request()->has('completed')?'complete':'incomplete';
    	//$task->method();

    	// $task->update([
    	// 	'completed'=>request()->has('completed')
    	// ]);

    	return back();
    }

}

<?php

namespace App\Http\Controllers;

use App\Project;
use App\Mail\ProjectCreated;
//use App\Services\Twitter;
use Illuminate\Http\Request;
//use Illuminate\Filesystem\Filesystem;

class ProjectsController extends Controller
{
    // public function __construct 
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$projects = Project::where('owner_id', auth()->id())->get();//owner_id not found

        $projects = Project::all();
        // $projects = auth()->user()->projects;
        //return $projects;
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        Project::create(request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]));

        //$project = $attributes['owner_id'] = auth->id();
        //Project::create($attributes);

        \Mail::to('jeffrey@laracasts.com')->send();
        new ProjectCreated($project);

        return redirect('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id/*, Filesystem $file*/)   //, Twitter $twitter
    {
        //dd($file);
        //$filesystem = app('Illuminate\Filesystem\Filesystem'); //instance of FileSystem 
        //dd($filesystem);
        //$twitter = app('twitter'); //apelarea service-continerului $twitter
        //dd($twitter);

        //$this->authorize('view', $id); -legat de policy -nu merge
        try{
            $id = decrypt($id);
        } catch (DecryptException $e) {
            abort(403, __('app.invalid_data_received'));
        }

        $project = Project::findOrFail($id);
        return view('projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $id = decrypt($id);
        } catch (DecryptException $e) {
            abort(403, __('app.invalid_data_received'));
        }
        $project = Project::find($id);
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //dd(request()->all());
        request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $project = Project::find($id);
        $project->title = request('title');
        $project->description = request('description');
        $project->save();
        return redirect('/projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();
        //$project->delete();
        
        return redirect('/projects');
    }

}

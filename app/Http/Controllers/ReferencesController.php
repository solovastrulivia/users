<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reference;

class ReferencesController extends Controller
{

    public function getOptionsList(Request $query)
    {
        $options = new Reference;
        $options = $options->where('type', $query->type);
        if($query->has('parent') )
        {
            $options = $options->where('parent', $query->parent);
        }
        $options = $options->orderBy('label')->get()->pluck('label', 'value');
       
        return response()->json($options);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $judete = Reference::where('type', 'judet')->orderBy('label')->pluck('label', 'value');
        //dd($judete);
        $localitati = Reference::where('type', 'localitate')->where('parent', @$profile->judet)->orderBy('label')->pluck('label', 'value');


        return view('references.create', compact('judete', 'localitati'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserCalendar;
use App\Schedule;
use Mail;

class ScheduleController extends Controller
{
    public function actionStep1($id)
    {   
        try{
            $user_calendar_id = decrypt($id);
        } catch (DecryptException $e) {
            abort(403, __('app.invalid_data_received'));
        }
        
    	$schedules = Schedule::all();
    	//dd($schedules);
    	
 
    	$user_c = UserCalendar::findOrFail($user_calendar_id);
    	//dd($user_c);

    	$disponibilities = [];
        $tempFrom = $user_c->from;  
        //dd($tempFrom); 
        //dd(strtotime($user_c->to), strtotime($tempFrom));
        //dd(strtotime($user_c->to)- strtotime($tempFrom));
        //dd((strtotime($user_c->to)- strtotime($tempFrom)) / 60); //240 minute tot intervalul de timp
        $key=0;$indisponibil=[];
        while( (strtotime($user_c->to) - strtotime($tempFrom)) / 60 > 0){
            $disponibilities[$key] = date('H:i', strtotime($tempFrom)) . '-' . date('H:i', strtotime('+30 minutes', strtotime($tempFrom)));
            foreach($schedules as $schedule){
    		//var_dump($schedule->ucalendar_id."<br>"); 
	    		if($schedule->ucalendar_id==$user_c->id && $schedule->date_in==$user_c->date_in && $schedule->interval==$disponibilities[$key]) {
	    			$indisponibil[$key] = true;
	    		}
	    	}
	    	$key++;
            $tempFrom = date('Y-m-d H:i:s', strtotime('+30 minutes', strtotime($tempFrom)));
        }
        //dd($disponibilities, $indisponibil);

    	return view('schedule.action_step1', compact('disponibilities', 'indisponibil','user_c'));
    }

    public function storeStep1(Request $request)
    {
    	//dd($request);
    	$userc = UserCalendar::findOrFail($request->id);
    	//dd($userc); 
    	request()->validate([
    		'id' => 'required',
            'date_in' => 'required',
            'disponibility' => 'required',    
             
    	]);

    	Schedule::create([
			'ucalendar_id'=>$userc->id,
			'date_in'=> ($request->date_in==$userc->date_in)?$userc->date_in:null,
			'interval' => $request->disponibility
		]);

    	// Schedule::create(request()->validate([
     //        'ucalendar_id' => 'required',
     //        'date_in' => 'required',
     //        'interval' => 'required',
     //    ]));

    	return view('schedule.send_email');
 	}

 	public function sendEmail(Request $request)
 	{
 		//dd($request->all());

 		//$data = ['mail_message' => 'Domnul/Doamana '. $user->name .' a facut cererea de programare in data de ' . substr($fromDate, 0, -8) .' in intervalul orar ' . $disponibility . ' avand urmatorul motiv: '. $request->user_message];
        $this->sendingEmail('schedule.email', ['message' => 'mesaj'] , 'liviutza09@yahoo.com', 'aaa', 'solovastrulivia@yahoo.com', 'livia', 'Cerere programare');
 	}

 	public function sendingEmail($view_file, $data, $email_from, $name_from, $mail_to, $name_to, $subject){
        Mail::send($view_file, $data, function ($mail) use ($email_from, $name_from, $mail_to, $name_to, $subject) {
            $mail->from($email_from, $name_from)->to($mail_to, $name_to)->subject($subject);
        });
    }

    public function toEmail()
    {
        $message = '';
        return view('schedule.email', compact('message'));
    }

}

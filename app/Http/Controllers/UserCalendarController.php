<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserCalendar;

class UserCalendarController extends Controller
{
    public function show()
    {
    	
    	$days_appoitments = UserCalendar::orderBy('date_in')->get();
    	//dd($days_appoitments);

    	return view('calendar.user_calendar', compact('days_appoitments'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class jQueryController extends Controller
{
    public function allEx(){
    	return view('jQuery.jquery_all');
    } 

    public function jQueryEx1(){
    	return view('jQuery.jquery_ex1');
    }

    public function showSole1(){
    	return view('jQuery.sole_first');
    }

    public function showSole2(){
    	return view('jQuery.sole_second');
    }

    public function showMarket(){
    	return view('jQuery.madison_square');
    }

    public function introEffect(){
        return view('jQuery.intro_effect');
    }

    public function hideImage(){
        return view('jQuery.hide_image');
    }
     public function onEvent(){
        return view('jQuery.on_event');
    }


    public function validateForm(){
        return view('jQuery.form');
    }
    public function validateFormPost(Request $request){
        dd($request->all());
    }
    
    public function wschools(){
        return view('jQuery.w3schools');
    }
}

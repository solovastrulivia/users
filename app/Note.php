<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $guarded =[];
 
    public function Course(){
        return $this->belongsTo('App\Course', 'course_id', 'id'); //invese in many to many
        //return $this->belongsToMany('App\User');
    }

    public function Student(){
        return $this->belongsTo('App\Student', 'student_id', 'id'); //invese in many to many
        //return $this->belongsToMany('App\User');
    }
}
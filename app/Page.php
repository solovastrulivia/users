<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
     protected $fillable = ['user_id', 'title', 'author', 'chapters', 'no_pages_day'];
     public $timestamps=false;

     public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes; 

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');//sau the_user_id (user_id este implicit)
    }

    public function photos() {
    	return $this->morphMany('App\Photo', 'imageable');
    }

    public function tags() {
    	return $this->morphToMany('App\Tag', 'taggable');
    }
}

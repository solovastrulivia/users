<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['company_id','products_name', 'products_no', 'profile', 'for_sale'];
    public $timestamps=false;

    public function company()
{
    return $this->belongsTo('App\Company', 'company_id', 'id');
}

public function characteristic()
	{
		return $this->hasOne('\App\Characteristic', 'product_id', 'id');
	}
}

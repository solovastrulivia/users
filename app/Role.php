<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User'); //invese in many to many
        //return $this->belongsToMany('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	protected $primaryKey = 'id';
    protected $fillable = [
        'ucalendar_id', 'date_in', 'interval', 'created_at', 'updated_at'
    ];    

    public function userSchedule(){
        return $this->belongsTo(UserCalendar::class);
    }
}

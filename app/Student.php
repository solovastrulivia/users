<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded =[];

    public function Notes()
    {
        return $this->hasMany('App\Note', 'id', 'student_id');
    }

}

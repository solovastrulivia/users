<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\UserResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'age', 'company_id'
    ];
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function roles(){
    //     return $this->belongsToMany(Role::class)->withTimestamps();
    // }

    public function details()
    {
        return $this->hasOne('App\Detail', 'user_id', 'id');
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    public function appoitments()
    {
        return $this->hasMany('App\UserCalendar', 'userc_id', 'id');
    }

    // public function post()
    // {
    //      return $this->hasOne('App\Post', 'user_id', 'id');//sau the_user_id (user_id este implicit)
    // }

    public function posts()
    {
        return $this->hasMany('App\Post');//sau the_user_id (user_id este implicit)
    }

    public function roles(){
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id')->withPivot('created_at');

        //To customize tables name and columns folow the format below
        //return $this->belongsToMany('App\Role', 'user_roles', 'user_id', 'role_id');
    }

    public function photos() {
        return $this->morphMany('App\Photo', 'imageable');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function address(){ ///corect e Address respectand standardele
        return $this->hasOne('App\Address');
    }
}

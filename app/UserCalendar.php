<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCalendar extends Model
{
	protected $table = 'user_calendar';
	protected $primaryKey = 'id';
    protected $fillable = [
        'userc_id', 'date_in', 'from', 'to'
    ];    
    public $timestamps = false;

    public function userAppoitments(){
        return $this->belongsTo(User::class);
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule', 'ucalendar_id', 'id');
    }
}

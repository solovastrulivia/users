<?php


// connect to DB
$old = new mysqli('127.0.0.1', 'root', '', 'telemedicina');
if($old === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$new = new mysqli('127.0.0.1', 'root', '', 'users');
if($new === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}


//seed_refvalues.php
// -------------------- REF VALUES ----------------------------------
//empty refvalues table
$new->query('truncate references');

// insert root data 
// judet      
$new->query("INSERT INTO references (
    type,
    value,
    label
    ) VALUES (
    '_root',
    'judet',
    'Judet'
    )"
) or die(mysqli_error($new) . 'judet (root)');

//judet
$sql = "Select * from refvalues where type = 'judet'";

 $old_ref = $old->query("Select * from refvalues where type='judet'");

if($old_ref = $old->query("Select * from refvalues where type='judet'")){
while($item =  mysqli_fetch_object($old_ref)){
    // insert data
    $new->query("INSERT INTO references (
                type,
                value,
                label,
                opt1
            ) VALUES (
                '" . $new->real_escape_string($item->type) . "',
                '" . $new->real_escape_string($item->value) . "',
                '" . $new->real_escape_string($item->label) . "',
                '" . $new->real_escape_string($item->opt1) . "'
            )"
        ) or die(mysqli_error($new) . 'judet' );
    }
}

//insert root data 
//localitate      
$new->query("INSERT INTO references (
    type,
    value,
    label
    ) VALUES (
    '_root',
    'localitate',
    'Localitate'
    )"
) or die(mysqli_error($new) . 'localitate (root)');

//localitate 
if($old_ref = $old->query("Select * from refvalues where type='localitate'")){
    while($item =  mysqli_fetch_object($old_ref)){
        //var_dump($item);
        //insert data
        $new->query("INSERT INTO references (
                    type,
                    parent,
                    value,
                    label,
                    opt1
                ) VALUES (
                    '" . $new->real_escape_string($item->type) . "',
                    '" . $new->real_escape_string($item->parent) . "',
                    '" . $new->real_escape_string($item->value) . "',
                    '" . $new->real_escape_string($item->label) . "',
                    '" . $new->real_escape_string($item->opt1) . "'
                )"
            ) or die(mysqli_error($new) . 'localitate' );
        }
    }

    //insert root data 
//status      
// $new->query("INSERT INTO references (
//     type,
//     value,
//     label
//     ) VALUES (
//     '_root',
//     'status',
//     'Status'
//     )"
// ) or die(mysqli_error($new) . 'status (root)');

// $old_ref = [ 'quote', 'ordered', 'in_production', 'pending', 'delivered', 'canceled'];
// foreach($old_ref as $item) {
//     $new->query("INSERT INTO references (
//                     type,
//                     value,
//                     label
//                 ) VALUES (
//                     'status',
//                     '" . $new->real_escape_string($item) . "',
//                     '" . ucfirst($new->real_escape_string($item)) . "'
//                 )"
//                ) or die(mysqli_error($new) . 'status');
// }



mysqli_close($new);
mysqli_close($old);

echo 'Done.';
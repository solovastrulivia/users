<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'name' => 'User',
    'calendar' => 'Calendar',
    'ajax_example' => 'Ajax example',
    'submit' => 'Programeaza',
    'email' => 'Email',
    'blacklist' => 'Blacklist',
    'age' => 'Age',
    'added_date' => 'Added date',
    'options' => 'Options',
    'invalid_data_received' => 'Invalid data received',

    'county' => 'Judet',
    'city' => 'Localitate',
    'students' => 'Elevi',
    'actiuni' => 'Actiuni',
    'note' => 'Note',
    'materie' => 'Materie',
    'nota' => 'Nota',
    'quantity' => 'Cantitate',
    'books' => 'Carti',
    'price' => 'Pret',
    'total' => 'Total',
    'title' => 'Titlu',
    'save' => 'Salveaza',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];
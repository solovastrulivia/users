@extends('layouts.app')

@section('content')

<div class="container border border-info rounded  bg-light p-3 text-center">

	<h2>Examples using ajax</h2>
	<div class="row py-2 px-5">
			<a href="http://localhost/users/ajax/ex1" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Example 1</a>
			<a href="http://localhost/users/ajax/ex2" class="btn btn-primary rounded btn-block px-5 py-2 m-1">Example 2</a>
			<a href="http://localhost/users/ajax/search-pages" class="btn btn-success rounded btn-block px-5 py-2 m-1">Searching on pages</a>
			<a href="http://localhost/users/ajax/utilizare" class="btn btn-primary rounded btn-block px-5 py-2 m-1">Show utilizare</a>
			<a href="http://localhost/users/ajax/ajax-crud/index" class="btn btn-success rounded btn-block px-5 py-2 m-1">CRUD using AJAX</a>
			<a href="http://localhost/users/ajax-products" class="btn btn-primary rounded btn-block px-5 py-2 m-1">CRUD for products using AJAX</a>

	</div>	
</div>



@endsection

@extends('layouts.app')

@section('content')

<div class="container bg-light border border-primary rounded p-5">
	<div class="row">
        <!-- AJAX EXAMPLE -->
         <h3 class="text-center">{{Lang::get('app.ajax_example') }}</h3>
        <div class="col-md-12">
	        <div id = 'msg'>This message will be replaced using Ajax. 
	         Click the button to replace the message.</div>

		    <?php
		        echo Form::button('Replace Message',['onClick'=>'getMessage()']);
		    ?>
      	</div>
    </div>	
</div>

@endsection

@section('footer-js')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      
      <script>
         function getMessage() {
            $.ajax({
               type:'POST',
               url:'/users/ajax/ex1'+'?_token=' + '{{ csrf_token() }}',
               success:function(data) {
                  $("#msg").html(data.msg);
               }
            });
         }
      </script>
@endsection
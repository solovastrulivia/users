<div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h2 class="modal-title mx-5">Add a new book</h2>
        </div>

        <!-- Modal body -->
        
        {{Form::open(['url' => route('storeModal')]) }}
        <div class="modal-body">

            <div class="row my-2 mx-5">
                <div class="col-4">
                    User id:
                </div>
                <div class="col-8">
                    {{ Form::select('user_id', $user_name,  null, ['class' => 'browser-default custom-select', 'placeholder' => __('-select-') ]) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Title:
                </div>
                <div class="col-8">
                    {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Author:
                </div>
                <div class="col-8">
                    {{ Form::text('author', null, ['class' => 'form-control', 'id' => 'author']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Chapters:
                </div>
                <div class="col-8">
                    {{ Form::text('chapters', null, ['class' => 'form-control', 'id' => 'chapters']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Pages:
                </div>
                <div class="col-8">
                    {{ Form::number('no_pages_day', null, ['class' => 'form-control', 'id' => 'no_pages_day']) }}
                </div>
            </div>
                
        </div>

        <!-- Modal footer -->
        <div class="modal-checkout-footer d-flex justify-content-between">
            <button type="button" class="btn btn-danger pull-left btn-rounded btn-sm btn-small-text my-3 mx-5" data-dismiss="modal">{{ __('Cancel') }}</button>
            {{ Form::submit('Store', ['class' => 'btn btn-primary pull-right btn-rounded btn-sm btn-small-text my-3 mx-5' ]) }}
        </div>
        {{Form::close()}}
    </div>
</div>
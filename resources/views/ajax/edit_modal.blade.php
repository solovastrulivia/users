<div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h2 class="modal-title mx-5">Edit book</h2>
        </div>

        <!-- Modal body -->
        
        {{Form::open(['url' => route('updateModal', $page->id)]) }}
        <div class="modal-body">
            
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Title:
                </div>
                <div class="col-8">
                    {{ Form::text('title', $page->title, ['class' => 'form-control', 'id' => 'title']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Author:
                </div>
                <div class="col-8">
                    {{ Form::text('author', $page->author, ['class' => 'form-control', 'id' => 'author']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Chapters:
                </div>
                <div class="col-8">
                    {{ Form::text('chapters', $page->chapters, ['class' => 'form-control', 'id' => 'chapters']) }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Pages:
                </div>
                <div class="col-8">
                    {{ Form::number('no_pages_day', $page->no_pages_day, ['class' => 'form-control', 'id' => 'no_pages_day']) }}
                </div>
            </div>
                
        </div>

        <!-- Modal footer -->
        <div class="modal-checkout-footer d-flex justify-content-between">
            <button type="button" class="btn btn-danger pull-left btn-rounded btn-sm btn-small-text my-3 mx-5" data-dismiss="modal">{{ __('Cancel') }}</button>
            {{ Form::submit('Update', ['class' => 'btn btn-primary pull-right btn-rounded btn-sm btn-small-text my-3 mx-5', 'method' => 'post']) }}
        </div>
        {{Form::close()}}
    </div>
</div>
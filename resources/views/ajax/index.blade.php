@extends('layouts.app')

@section('content')

<div class="container box bg-light border border-primary rounded p-5">
	<h3 class="text-center">Modal</h3>

	<div class="panel panel-default">
			<a href="" class="btn btn-info btn-sm btn-rounded pull-right modal-create mt-2 mr-2"> Add a book </a>
			<!-- Hidden Button to Open the Modal -->
		     <button type="button" class="btn-modal-create hidden" data-toggle="modal" data-target="#createModal"></button>
		    <!-- The Modal -->
		    <div class="modal" id="createModal"></div>
		<div class="panel-default">

			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<th>Title</th>
						<th>Author</th>
						<th>Chapters</th>
						<th>No. of pages</th>
						<th>Actions</th>
					</thead>
					<tbody>
						@foreach($pages as $page)
							<tr>
								<td>{{ $page->title }}</td>
								<td>{{ $page->author }}</td>
								<td>{{ $page->chapters }}</td>
								<td>{{ $page->no_pages_day }}</td>
								<td>
									<a href="" class="btn btn-success btn-sm btn-rounded modal-info" data-id='{{ $page->id }}'> View </a>
									<a href="" class="btn btn-primary btn-sm btn-rounded modal-edit" data-id='{{ $page->id }}'> Edit </a>
									<form action="{{ route('deleteModal', $page->id) }}" method="POST" class="d-inline">
                                       @method('DELETE')
                                        @csrf
                                        <button class="btn btn-sm btn-danger">{{ __('Delete') }}</button>
                                    </form> 
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

	<!-- MODAL INFO -->
    <!-- Hidden Button to Open the Modal -->
    <button type="button" class="btn-modal-info hidden" data-toggle="modal" data-target="#infoModal"></button>
    <button type="button" class="btn-modal-edit hidden" data-toggle="modal" data-target="#editModal"></button>
    <!-- The Modal -->
    <div class="modal" id="infoModal"></div>
    <div class="modal" id="editModal"></div>
@endsection

@section('footer-js')
<script>

$(() => {

        // Open emi/res modal
    $(document).on('click', '.modal-info', function(e){
        e.preventDefault();
        $.ajax({
            url: '{{route('modal')}}/?id=' + $(this).data('id'),
            dataType: 'html',
            type: 'GET',
            success: function(response){
                    $('#infoModal').html(response);
                    $('.btn-modal-info').click();
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });
    
    // END JQUERY READY
});

$(() => {

        // Open emi/res modal
    $(document).on('click', '.modal-edit', function(e){
        e.preventDefault();
        $.ajax({
            url: '{{route('editModal')}}/?id=' + $(this).data('id'),
            dataType: 'html',
            type: 'GET',
            success: function(response){
                    $('#editModal').html(response);
                    $('.btn-modal-edit').click();
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });
    
    // END JQUERY READY
});

$(() => {

    // Open emi/res modal
    $(document).on('click', '.modal-create', function(e){
        e.preventDefault();
        $.ajax({
            url: '{{route('createModal')}}',
            dataType: 'html',
            type: 'GET',
            success: function(response){
                    $('#createModal').html(response);
                    $('.btn-modal-create').click();
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    });
    
    // END JQUERY READY
});

</script>
@endsection
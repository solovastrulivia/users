<div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
            <h2 class="modal-title mx-5">Book details</h2>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        
        <div class="modal-body">
        
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Title:
                </div>
                <div class="col-8">
                    {{ $page->title }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Author:
                </div>
                <div class="col-8">
                    {{ $page->author }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Chapters:
                </div>
                <div class="col-8">
                    {{ $page->chapters }}
                </div>
            </div>
            <div class="row my-2 mx-5">
                <div class="col-4">
                    Pages:
                </div>
                <div class="col-8">
                    {{ $page->no_pages_day }}
                </div>
            </div>
                
        </div>

        <!-- Modal footer -->
        <div class="modal-checkout-footer d-flex justify-content-between">
            <button type="button" class="btn btn-danger pull-right btn-rounded btn-sm btn-small-text my-3 mx-5" data-dismiss="modal">{{ __('Cancel') }}</button>
        </div>

    </div>
</div>
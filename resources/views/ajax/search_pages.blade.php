@extends('layouts.app2')

@section('content')

<div class="container box bg-light border border-primary rounded p-5">
	<h3 class="text-center">Live search in Laravel using AJAX</h3>

	<div class="panel panel-default">
		<div class="panel-default">
			<div class="panel-heading">Search pages data</div>
			<div class="panel-body">
				<input type="text" name="search" id="search" class="form-control" placeholder="Search pages data" />

				<div class="table-responsive">
					<h3 class="text-center py-3">Total data: <span id="total_records"></span></h3>
					<table class="table table-striped table-bordered">
						<thead>
							<th>Title</th>
							<th>Author</th>
							<th>Chapters</th>
							<th>No. of pages</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('footer-js')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
      
    <script>
    $(document).ready(function(){
    	fetch_pages_data();
      	function fetch_pages_data(query = '') {
            $.ajax({
               url:"{{ route('ajax.searchPagesAction') }}",
               method: 'GET',
               data:{query:query},
               dataType:'json',
               success:function(data) {
                  $('tbody').html(data.table_data);
                  $('#total_records').text(data.total_data);
               }
            })
        }

        $(document).on('keyup', '#search', function(){
	    	var query = $(this).val();
	    	fetch_pages_data(query);
	    });
    });
         
    </script>
@endsection
@extends('layouts.app2')

@section('content')

<div class="container box bg-light border border-primary rounded p-5">
	<h3 class="text-center">Displayng a table in Laravel using AJAX</h3>

	<div class="panel panel-default">
		<div class="panel-default">
			<div class="panel-heading">Show data from utilizare</div>
			<div class="panel-body">
				<div class="table-responsive">
					<h3 class="text-center py-3">Total data: <span id="total_records"></span></h3>
					<table class="table table-striped table-bordered">
						<thead>
							<th>Denumire</th>
							<th>Denumire stiintifica</th>
							<th>Indicatii</th>
							<th>Mod utilizare</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('footer-js')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
      
    <script>
    $(document).ready(function(){
    	show_data();
      	function show_data() {
            $.ajax({
               url:"{{ route('ajax.showUtilizareAction') }}",
               method: 'GET',
              
               dataType:'json',
               success:function(data) {
                  $('tbody').html(data.table_data);
                  $('#total_records').text(data.total_data);
               }
            })
        }

    });
         
    </script>
@endsection
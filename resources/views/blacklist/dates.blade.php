<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<!-- <link rel="stylesheet" href="{{ asset('css/lib/datatables/datatables.min.css') }}"> -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- <script type="text/javascript" src="{{ asset('js/lib/datatables/datatables.min.js') }}"></script> -->
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
	<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.jqueryui.min.js"></script> -->

	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
 	-->
    

	<script>
		// $(document).ready( function () {
  //   		$('#table_id').DataTable();
		// } );

		$(() => {

        // DATATABLE initialization
        let datatable = $('#table_id').DataTable( {
            pagingType: "full_numbers",
            responsive: true,
            processing: true,
            serverSide: true,
            async     : false,
            "ajax": {
                "url":  "{{ route('blacklist.dataTable2') }}"
            },
            "lengthMenu": [[10, 25, 50], [10, 25, 50]],
            "columns": [
                { "data": "name" },
                { "data": "email" },
                { "data": "age" }
            ],
            "columnDefs": [
                { "orderable": false, "targets": [0, 1, 2] },
                { "searchable": false, "targets": [0, 1, 2] }
            ],
            "order": [[ 0, "asc" ]],
            initComplete: function(settings, json) {
                errorCount = 0;
              }
        } );

    let errorCount = 0;
        // IF ajax error, no alert, for 3 times draw again datatable and/or console log error
        $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
            if(errorCount <= 3) {
                errorCount++;
                datatable.clear().draw(); 
//            console.log(message);
            } else {
                errorCount = 0;
                console.log(message);
            }
        }
        
    // END JQUERY READY
    });

	</script>

	<title>DataTable</title>
</head>
<body >
	<h2 class="text tex-info text-center">Dates with DataTable</h2>
	<table id="table_id" class="display table" width="100%">
            <thead>
                <tr>
                    <th>{{ __('app.name') }} <i class="fa fa-search text-primary" aria-hidden="true"></i></th>
                    <th>{{ __('app.email') }} <i class="fa fa-search text-primary" aria-hidden="true"></i></th>
                    <th>{{ __('app.age') }}</th>
                </tr>
            </thead>
            <tbody>
            <!-- DATA TO DISPLAY -->
            </tbody>
            <tfoot>
                <tr>
                    <th>{{ __('app.name') }}</th>
                    <th>{{ __('app.email') }}</th>
                    <th>{{ __('app.age') }}</th>
                </tr>
            </tfoot>
    <!-- <thead>
        <tr>
            <th>Column 1</th>
            <th>Column 2</th>
            <th>Column 3</th>
            <th>Column 4</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 01 Data 1</td>
            <td>Row 01 Data 2</td>
            <td>Row 01 Data 3</td>
            <td>Row 01 Data 4</td>
        </tr>
        <tr>
            <td>Row 02 Data 1</td>
            <td>Row 02 Data 2</td>
            <td>Row 02 Data 3</td>
            <td>Row 02 Data 4</td>
        </tr>
        <tr>
            <td>Row 03 Data 1</td>
            <td>Row 03 Data 2</td>
            <td>Row 03 Data 3</td>
            <td>Row 03 Data 4</td>
        </tr>
        <tr>
            <td>Row 04 Data 1</td>
            <td>Row 04 Data 2</td>
            <td>Row 04 Data 3</td>
            <td>Row 04 Data 4</td>
        </tr>
        <tr>
            <td>Row 05 Data 1</td>
            <td>Row 05 Data 2</td>
            <td>Row 05 Data 3</td>
            <td>Row 05 Data 4</td>
        </tr>
        <tr>
            <td>Row 6 Data 1</td>
            <td>Row 6 Data 2</td>
            <td>Row 6 Data 3</td>
            <td>Row 6 Data 4</td>
        </tr>
        <tr>
            <td>Row 7 Data 1</td>
            <td>Row 7 Data 2</td>
            <td>Row 7 Data 3</td>
            <td>Row 7 Data 4</td>
        </tr>
        <tr>
            <td>Row 8 Data 1</td>
            <td>Row 8 Data 2</td>
            <td>Row 8 Data 3</td>
            <td>Row 8 Data 4</td>
        </tr>
        <tr>
            <td>Row 9 Data 1</td>
            <td>Row 9 Data 2</td>
            <td>Row 9 Data 3</td>
            <td>Row 9 Data 4</td>
        </tr>
        <tr>
            <td>Row 10 Data 1</td>
            <td>Row 10 Data 2</td>
            <td>Row 10 Data 3</td>
            <td>Row 10 Data 4</td>
        </tr>
        <tr>
            <td>Row 11 Data 1</td>
            <td>Row 11 Data 2</td>
            <td>Row 11 Data 3</td>
            <td>Row 11 Data 4</td>
        </tr>
        <tr>
            <td>Row 12 Data 1</td>
            <td>Row 12 Data 2</td>
            <td>Row 12 Data 3</td>
            <td>Row 12 Data 4</td>
        </tr>
        <tr>
            <td>Row 13 Data 1</td>
            <td>Row 13 Data 2</td>
            <td>Row 13 Data 3</td>
            <td>Row 13 Data 4</td>
        </tr>
        <tr>
            <td>Row 14 Data 1</td>
            <td>Row 14 Data 2</td>
            <td>Row 14 Data 3</td>
            <td>Row 14 Data 4</td>
        </tr>
        <tr>
            <td>Row 15 Data 1</td>
            <td>Row 15 Data 2</td>
            <td>Row 15 Data 3</td>
            <td>Row 15 Data 4</td>
        </tr>
        <tr>
            <td>Row 16 Data 1</td>
            <td>Row 16 Data 2</td>
            <td>Row 16 Data 3</td>
            <td>Row 16 Data 4</td>
        </tr>
        <tr>
            <td>Row 17 Data 1</td>
            <td>Row 17 Data 2</td>
            <td>Row 17 Data 3</td>
            <td>Row 17 Data 4</td>
        </tr>
        <tr>
            <td>Row 18 Data 1</td>
            <td>Row 18 Data 2</td>
            <td>Row 18 Data 3</td>
            <td>Row 18 Data 4</td>
        </tr>
        <tr>
            <td>Row 19 Data 1</td>
            <td>Row 19 Data 2</td>
            <td>Row 19 Data 3</td>
            <td>Row 19 Data 4</td>
        </tr>
        <tr>
            <td>Row 20 Data 1</td>
            <td>Row 20 Data 2</td>
            <td>Row 20 Data 3</td>
            <td>Row 20 Data 4</td>
        </tr>
        <tr>
            <td>Row 22 Data 1</td>
            <td>Row 22 Data 2</td>
            <td>Row 22 Data 3</td>
            <td>Row 22 Data 4</td>
        </tr>
        <tr>
            <td>Row 23 Data 1</td>
            <td>Row 23 Data 2</td>
            <td>Row 23 Data 3</td>
            <td>Row 23 Data 4</td>
        </tr>
        <tr>
            <td>Row 24 Data 1</td>
            <td>Row 24 Data 2</td>
            <td>Row 24 Data 3</td>
            <td>Row 24 Data 4</td>
        </tr>
        <tr>
            <td>Row 25 Data 1</td>
            <td>Row 25 Data 2</td>
            <td>Row 25 Data 3</td>
            <td>Row 25 Data 4</td>
        </tr>
        <tr>
            <td>Row 26 Data 1</td>
            <td>Row 26 Data 2</td>
            <td>Row 26 Data 3</td>
            <td>Row 26 Data 4</td>
        </tr>
        <tr>
            <td>Row 27 Data 1</td>
            <td>Row 27 Data 2</td>
            <td>Row 27 Data 3</td>
            <td>Row 27 Data 4</td>
        </tr>
        <tr>
            <td>Row 27 Data 1</td>
            <td>Row 27 Data 2</td>
            <td>Row 27 Data 3</td>
            <td>Row 27 Data 4</td>
        </tr>
        <tr>
            <td>Row 28 Data 1</td>
            <td>Row 28 Data 2</td>
            <td>Row 28 Data 3</td>
            <td>Row 28 Data 4</td>
        </tr>
        <tr>
            <td>Row 29 Data 1</td>
            <td>Row 29 Data 2</td>
            <td>Row 29 Data 3</td>
            <td>Row 29 Data 4</td>
        </tr>
        <tr>
            <td>Row 30 Data 1</td>
            <td>Row 30 Data 2</td>
            <td>Row 30 Data 3</td>
            <td>Row 30 Data 4</td>
        </tr>
    </tbody> -->
</table>
</body>
</html>
@extends('layouts.app')

@section('content')
    <div class="box-typical box-typical-padding">
        
        <h2 class="with-border">{{ __('app.blacklist') }}
            <!-- a href="{ route('blacklist.create') }}" class="btn btn-sm btn-rounded btn-primary pull-right">{ __('app.add_to_blacklist') }}</a> -->
        </h2>

        <div class="site-header-search-container pull-right m-5">
            <form class="site-header-search closed">
                <input type="text" placeholder="Search"/>
                <button type="submit">
                    <span class="font-icon-search"></span>
                </button>
                <div class="overlay"></div>
            </form>
        </div>

        <table id="tableData" class="display" width="100%">
            <thead>
                <tr>
                    <th>{{ __('app.name') }} <i class="fa fa-search text-primary" aria-hidden="true"></i></th>
                    <th>{{ __('app.email') }} <i class="fa fa-search text-primary" aria-hidden="true"></i></th>
                    <th>{{ __('app.age') }}</th>
                </tr>
            </thead>
            <tbody>
            <!-- DATA TO DISPLAY -->
            </tbody>
            <tfoot>
                <tr>
                    <th>{{ __('app.name') }}</th>
                    <th>{{ __('app.email') }}</th>
                    <th>{{ __('app.age') }}</th>
                </tr>
            </tfoot>
        </table>
        
    </div>
@endsection

@section('footer-js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

<!-- <link rel="stylesheet" href="{{ asset('css/lib/datatables/datatables.min.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <!-- <script type="text/javascript" src="{{ asset('js/lib/datatables/datatables.min.js') }}"></script> -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.jqueryui.min.js"></script> -->

<script>
    
    $(() => {

        // DATATABLE initialization
        let datatable = $('#tableData').DataTable( {
            pagingType: "full_numbers",
            responsive: true,
            processing: true,
            serverSide: true,
            async     : false,
            "ajax": {
                "url":  "{{ route('blacklist.dataTable') }}"
            },
            "lengthMenu": [[10, 25, 50], [10, 25, 50]],
            "columns": [
                { "data": "name" },
                { "data": "email" },
                { "data": "age" }
            ],
            "columnDefs": [
                { "orderable": false, "targets": [0, 1, 2] },
                { "searchable": false, "targets": [0, 1, 2] }
            ],
            "order": [[ 0, "asc" ]],
            initComplete: function(settings, json) {
                errorCount = 0;
              }
        } );

    let errorCount = 0;
        // IF ajax error, no alert, for 3 times draw again datatable and/or console log error
        $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
            if(errorCount <= 3) {
                errorCount++;
                datatable.clear().draw(); 
//            console.log(message);
            } else {
                errorCount = 0;
                console.log(message);
            }
        }
        
    // END JQUERY READY
    });

    </script>

@endsection

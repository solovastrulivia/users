@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center my-4">{{ __('app.books') }}</h2>

        <p>Aici se pot adauga elemente in tabel. </p>

            <?php $total = 0; ?>
            <table id="table-price" class="table w-100" style="font-size: 16px">
                <thead>
                    <tr class="">
                        <th class="">{{ __('app.title') }}</th>
                        <th class="">{{ __('app.quantity') }}</th>
                        <th class="">{{ __('app.price') }}</th>
                        <th class=" text-right">{{ __('app.total') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($books as $book)
                    <tr class="parent" data-id="{{$book->id}}">
                        <td>{{ $book->name }}</td>                 
                        <td>
                            <i class="fa fa-minus-circle minusq"></i> &nbsp;
                            <b class="cantitate">{{ $book->quantity }}</b>&nbsp; 
                            <i class="fa fa-plus-circle plusq"></i>
                        </td>
                        <td class="price-box">
                            <i class="fa fa-minus-circle minusp"></i> &nbsp;
                            <b class="price">{{ $book->price }}</b>&nbsp; 
                            <i class="fa fa-plus-circle plusp"></i>
                            
                            <span><i class="fa fa-plus change float-right"></i></span>
                        </td>
                        <td class="text-right total">{{ $book->quantity * $book->price }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="text-right"><b>Total</b></td>
                        <td class="text-right total-full"></td>
                    </tr>
            </tbody>
        </table>

        <fieldset class="add-content">
            <legend>Adaugare carti</legend>
            {{ Form::open( ['url' => route('books.storeBook'), 'class' => 'text-center border border-light p-1', 'id' => 'form-book'] ) }}
            <div class="add-book">
                <div class="row">
                    <div class="col-4">
                        {{ Form::text('name[0]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.title')]) }}
                    </div>
                    <div class="col-3">
                        {{ Form::text('quantity[0]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.quantity')]) }}
                    </div>
                    <div class="col-3">
                        {{ Form::text('price[0]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.price')]) }}
                    </div>
                    <div class="col-1 mt-4">
                        <i class="fa fa-minus-circle f18 delete-row"></i> &nbsp;
                        <i class="fa fa-plus-circle f18 add-row"></i>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="offset-10 col-2">
                    <button  type="submit" class="btn btn-sm btn-info" id="insert-book">{{ __('app.save') }}</button>
                    {{-- <button  type="button" class="btn btn-sm btn-info" id="insert-book">{{ __('app.save') }}</button> --}}
                </div>
            </div>
            {{ Form::close() }}

        </fieldset>
        
    </div>
@endsection

@section('footer-js')
<script>
    $(() => { 
        const input=`<input type="text" class="change_price" name="change_price">`;
        const priceBox=`<i class="fa fa-minus-circle minusp"></i> &nbsp;
                            <b class="price">%val</b>&nbsp; 
                            <i class="fa fa-plus-circle plusp"></i>
                            
                            
                            <span><i class="fa fa-plus change float-right"></i></span>`;
        calculate();

        $(document).on('click', '.minusq', function(){
        //$('.minusq').on('click', function(){
            let q;
            let item = $(this).closest('.parent').find('.cantitate');
            q = item.html();
            if(q > 0) q--;
            item.html(q);
            calculate();

        });

        $(document).on('click', '.plusq', function(){
        //$('.plusq').on('click', function(){
            let q;
            let item = $(this).closest('.parent').find('.cantitate');
            q = item.html();
            q++;
            item.html(q);
            calculate();

        });
        
        // $(document).on('click', '.minusp', function(){
        // //$('.minusp').on('click', function(){
        //     let p;
        //     let item = $(this).closest('.parent').find('.price');
        //     p = item.html();
        //     if(p > 0) p--;
        //     item.html(p);

        // });

        $(document).on('click', '.minusp', function(){
        //$('.minusp').on('click', function(){
            let p;
            let item = $(this).closest('.parent').find('.price');
            p = item.html();
            if(p > 0) p--;
            item.html(p);
        });

        // $.ajax({
        //     data: $('#form-book').serialize(),
        //     url: "{{ route('books.storeBook') }}",
        //     type: "POST",
        //     dataType: 'json',
        //     success: function () {
        //         console.log('Success');
        //         //$('#insert-book').html('Salvat');
        //     },
        //     error: function (data) {
        //         console.log('Error:', data);
        //         //$('#insert-book').html('Eroare');
        //     }
        // });

        $(document).on('click', '.plusp', function(){
        //$('.plusp').on('click', function(){
            let p;
            let item = $(this).closest('.parent').find('.price');
            p = item.html();
            p++;
            item.html(p);
            calculate();
        });

        $(document).on('click', '.change', function(){
            $(this).closest('.price-box').html(input);
            $('.change_price').focus();
        });

        // $('.change').on('click', function(){
        //     $(this).closest('.price-box').html(input);
        //     $('.change_price').focus();
        // });

        $(document).on('keyup', '.change_price', function (e) {
            if (e.keyCode === 13) {
                //alert('test');
                let val = $(this).val();
                $(this).closest('.price-box').html(priceBox.replace('%val', val));
                
            }
            calculate();
        });
    });
 
    function calculate(){
        let book, total=0;
        $('.parent').each(function(){
            book = parseFloat($(this).find('.cantitate').html()) * parseFloat($(this).find('.price').html());
            //console.log(book);
            total+=book;
            $(this).find('.total').html(book.toFixed(2));
        });
        $('.total-full').html(total.toFixed(2));
    }
   
    const addRowBook=$('.add-book').find('.row').first().prop('outerHTML');
    console.log(addRowBook);
    // const addRowBook=`
    //             <div class="row">
    //                 <div class="col-5">
    //                     {{ Form::text('name[%val]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.title')]) }}
    //                 </div>
    //                 <div class="col-3">
    //                     {{ Form::text('quantity[%val]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.quantity')]) }}
    //                 </div>
    //                 <div class="col-3">
    //                     {{ Form::text('price[%val]', null, ['class' => 'form-control m-2', 'placeholder' => __('app.price')]) }}
    //                 </div>
    //                 <div class="col-1 mt-4">
    //                     <i class="fa fa-minus-circle f18 delete-row"></i> &nbsp;
    //                     <i class="fa fa-plus-circle f18 add-row"></i>
    //                 </div>
    //             </div>`;

    //alert(addRowBook);

    $(document).on('click', '.add-row', function(){
        let count = 1;
        $('.add-book').append(addRowBook.replace('name[0]', `name[${count}]`).replace('quantity[0]', `quantity[${count}]`).replace('price[0]', `price[${count++}]`));
    });

    $(document).on('click', '.delete-row', function(){
        if($('.row').length > 1)
        $(this).closest('.row').remove();
    });

    // $(document).on('click', '#insert-book', function(){
        
    //     let books = []; //let id; let cantitate;let pret;
    //     let data = {};
    //     let index = 0;
    //     $('.parent').each(function(){
    //         //console.log('id: ' + $(this).data('id') + ' cantitate: ' + $(this).find('.cantitate').html() +' pret: ' + $(this).find('.price').html());
    //         data[0] = $(this).data('id');
    //         data[1] = parseFloat($(this).find('.cantitate').html());
    //         data[2] = parseFloat($(this).find('.price').html());
    //         //books[$(this).data('id')] = data;
    //         console.log(data[0], data[1], data[2]);
    //         books.push(data);
    //         console.log(books);
    //         // id = $(this).data('id');
    //         // cantitate = parseFloat($(this).find('.cantitate').html());
    //         // pret = parseFloat($(this).find('.price').html());
    //         //alert(id, cantitate, pret);
    //         // console.log(id, cantitate, pret);
    //         //books.push([data]);
    //         //console.log('IN', books, 'DATA:' + data);
    //         //break;
    //     });

    //     // for(let i =0; i<index;i++){
    //     //     for(let j=0;j<3;j++){
    //     //         console.log(`books[${i}][${j}]=`+books[i][j]);
    //     //     }
    //     // }
    //     console.log('BOOKS: ' +books);
    // });

</script>
@endsection

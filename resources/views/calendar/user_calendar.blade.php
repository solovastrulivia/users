@extends('layouts.app')

@section('content')
    
    <div class="box-typical box-typical-padding">
        <h3 class="with-border d-flex justify-content-center">{{ Lang::get('app.calendar') }}</h3>
        <div class="row">
            <!-- CALENDAR -->
            <div class="col-md-12 d-flex justify-content-center">
                <div id="month_calendar">

                    @foreach($days_appoitments as $appoitment)
                        @if($appoitment->date_in)
                            <a href="{{ route('schedule.actionStep1', encrypt($appoitment->id)) }}" class="btn btn-info text-center mx-5 my-1">
                            {{ $appoitment->from }} &nbsp;&nbsp;&nbsp;
                            {{ $appoitment->to }} &nbsp;&nbsp;&nbsp;
                            {{ $appoitment->date_in }}</a>
                            <br>
                        @endif                    
                    @endforeach
                    
                </div>
            </div>
            <!-- INPUT -->
            <!-- <div class="col-md-3">
                <div id="select_availability"></div>
            </div> -->
        </div>
    </div>

    
@endsection


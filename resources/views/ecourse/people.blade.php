@extends('layouts.app')
@section('content')
<div class="container">
    <h3>People</h3>
    @if(count($people))
        @foreach($people as $person)
            {{ $person }} <br>
        @endforeach
    @endif
    Number of persons: {{ count($people) }}
</div>
@endsection
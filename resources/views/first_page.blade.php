@extends('layouts.app')

@section('content')

<div class="container border border-info rounded  bg-light p-3" >

	<div class="d-flex justify-content-end">
		<div class="row">
			<div class="col-4 d-flex justify-content-end ">
				<i class="fas fa-flag"></i>
				<a href="{{ url('locale/en') }}" ><i class="fa fa-language"></i> &nbsp;EN&nbsp; </a>
				<a href="{{ url('locale/fr') }}" ><i class="fa fa-language"></i> &nbsp;FR&nbsp; </a>
				<a href="{{ url('locale/ro') }}" ><i class="fa fa-language"></i> &nbsp;RO&nbsp; </a>
				</ul>
			</div>
		</div>
	</div>
	<br>
	<h2 class="text-center">Meniu</h2>
	<h3 class="text-center text-info"> {{ __('messages.welcome') }}</h3>
	<div class="row py-2 px-5">
		<div class="col-sm-6 p-2">
			<a href="http://localhost/users/public/home" class="btn btn-success btn-block px-5 py-2 m-1">Home</a>
			<a href="http://localhost/users/public/welcome" class="btn btn-primary btn-block px-5 py-2 m-1">Welcome</a>
			<a href="http://localhost/users/public/myPage" class="btn btn-success btn-block px-5 py-2 m-1">My page</a>
			<a href="#" class="btn btn-primary btn-block px-5 py-2 m-1">Gallery</a>
			<a href="http://localhost/users/public/contact" class="btn btn-success btn-block px-5 py-2 m-1">Contact</a>
			<a href="http://localhost/users/public/references" class="btn btn-primary btn-block px-5 py-2 m-1">References</a>
			<a href="http://localhost/users/javascript/all-examples" class="btn btn-success btn-block px-5 py-2 m-1">JavaScript</a>
			<a href="http://localhost/users/index" class="btn btn-primary btn-block px-5 py-2 m-1">Modal</a>
			<a href="{{ route('posts.index') }}" class="btn btn-success btn-block px-5 py-2 m-1">Posts CRUD</a>
			<a href="http://localhost/users/blacklist/index" class="btn btn-primary btn-block px-5 py-2 m-1">DataTable</a>
		</div>
		<div class="col-sm-6 p-2">
			<a href="http://localhost/users/public/users" class="btn btn-success btn-block px-5 py-2 m-1">Users</a>
			<a href="http://localhost/users/public/companies" class="btn btn-primary btn-block px-5 py-2 m-1">Companies</a>
			<a href="http://localhost/users/public/products" class="btn btn-success btn-block px-5 py-2 m-1">Products</a>
			<a href="http://localhost/users/public/projects" class="btn btn-primary btn-block px-5 py-2 m-1">Projects</a>
			<a href="http://localhost/users/user-calendar/show" class="btn btn-success btn-block px-5 py-2 m-1">Schedules</a>
			<a href="http://localhost/users/ajax/all-examples" class="btn btn-primary btn-block px-5 py-2 m-1">Ajax</a>
			<a href="http://localhost/users/jQuery/all-examples" class="btn btn-success btn-block px-5 py-2 m-1">jQuery</a>
			<a href="{{ route('students.index') }}" class="btn btn-primary btn-block px-5 py-2 m-1">Studenti</a>
			<a href="{{ route('books.index') }}" class="btn btn-success btn-block px-5 py-2 m-1">Carti</a>
		</div>
	</div>
		
</div>
<!-- endsection -->
@stop
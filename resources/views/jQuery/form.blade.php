@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    
                     {{ Form::open( ['url' => route('jQuery.validateFormPost'), 'class' => 'text-center border border-light p-1', 'files' => true, 'id' => 'myform', 'required'] ) }}
                    <div class="col-lg-4 col-md-12 col-sm-12">
                    {{ Form::textarea('message', null, ['class' => 'form-control w-100', 'placeholder' => __('app.group_name') ]) }}
                    </div>
                    <button class="btn btn-info my-4 btn-block" id="myform" type="submit">{{ __('app.save') }}</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-js')
<script src="jquery-3.4.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$("#myform").validate({
  rules: {
    name: {
      required: true,
      minlength: 2
    }
  },
  messages: {
    message: {
      required: "We need your email address to contact you",
      minlength: jQuery.validator.format("At least {0} characters required!")
    }
  }
});
</script>
@endsection
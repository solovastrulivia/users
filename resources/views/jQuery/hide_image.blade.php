<!DOCTYPE html>
<html>
<head>
  <link rel='stylesheet' type='text/css' href='/users/public/css/css_image.css'>
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">

</head>
  
<body>

<table align='center'>
  <tr>
    <th class='header-small'>
      <div class='header-text'>Buttons</div>
    </th>
    <th class='header-big'>
      <div class='image-text'>Images</div>
    </th>
  </tr>
  
  <tr>
    <th class='button-col'>
      <button class="hide-button">
        <div class = 'button-text'>HIDE</div>
      </button>
      <br><br>
      <button class="show-button">
        <div class = 'button-text'>SHOW</div>
      </button>
      <br><br>
      <button class="toggle-button">
        <div class = 'button-text'>TOGGLE</div>
      </button>
    </th>
    <th class = 'img-col'>
      <img src = "https://s3.amazonaws.com/codecademy-content/courses/jquery/effects/veggies.jpg" class ="first-image">
    </th> 
  </tr>

  <tr>
    <th class='button-col'>
      <button class="fade-out-button">
        <div class = 'button-text'>FADE OUT</div>
      </button>
      <br><br>
      <button class="fade-in-button">
        <div class = 'button-text'>FADE IN</div>
      </button>
      <br><br>
      <button class="fade-toggle-button">
        <div class = 'button-text'>FADE TOGGLE</div>
      </button>
    </th>
    <th class = 'img-col'>
      <img src = "https://s3.amazonaws.com/codecademy-content/courses/jquery/effects/photo_2.jpg" class ="fade-image">
    </th> 
  </tr>

  <tr>
    <th class='button-col'>
      <button class="up-button">
        <div class = 'button-text'>SLIDE OUT</div>
      </button>
      <br><br>
      <button class="down-button">
        <div class = 'button-text'>SLIDE IN</div>
      </button>
      <br><br>
      <button class="slide-toggle-button">
        <div class = 'button-text'>SLIDE TOGGLE</div>
      </button>
    </th>
    <th class = 'img-col'>
      <img src = "https://s3.amazonaws.com/codecademy-content/courses/jquery/effects/photo_3.jpg" class ="slide-image">
    </th> 
  </tr>

</table>
  
  <script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<script> // src='js/hide.js
  $(document).ready(() => {
  $('.hide-button').on('click', () => {
    $('.first-image').hide()
  })
  $('.show-button').on('click', () => {
    $('.first-image').show();
  });
  $('.toggle-button').on('click', () => {
    $('.first-image').toggle();
  });

   $('.fade-out-button').on('click', () => {
    $('.fade-image').fadeOut(500);
  });
  $('.fade-in-button').on('click', () => {
     $('.fade-image').fadeIn(4000);
  });
  $('.fade-toggle-button').on('click', () => {
    $('.fade-image').fadeToggle('fast');
  });

  $('.up-button').on('click', () => {
    $('.slide-image').slideUp(100);
  });
  $('.down-button').on('click', () => {
    $('.slide-image').slideDown('slow');
  });
  $('.slide-toggle-button').on('click', () => {
    $('.slide-image').slideToggle(400);
  });
});
</script>   
  
  
</body>
</html>

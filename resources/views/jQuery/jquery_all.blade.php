@extends('layouts.app')

@section('content')

<div class="container border border-info rounded  bg-light p-3 text-center">

	<h2>Examples using jQuery</h2>
	<div class="row py-2 px-5">
			<a href="http://localhost/users/jQuery/ex1" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Example 1</a>
			<a href="http://localhost/users/jQuery/sole-first" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Sole Shoes login form</a>
			<a href="http://localhost/users/jQuery/sole-second" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Sole Shoes menu bar</a>
			<a href="http://localhost/users/jQuery/madison-market" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Madison Market Square Project</a>
			<a href="http://localhost/users/jQuery/intro-effects" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Introduction for jQuery effects</a>
			<a href="http://localhost/users/jQuery/intro-effects" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Introduction for jQuery effects</a>
			<a href="http://localhost/users/jQuery/form" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Validate form</a>
			<a href="http://localhost/users/jQuery/first-image-hide" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Hide images</a>
			<a href="http://localhost/users/jQuery/on-event" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Events</a>

			<a href="{{ route('jQuery.wschools') }}" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">w3shools</a>
	</div>	
</div>



@endsection

<!DOCTYPE html>
<html>
<head>
	<script src="jquery-3.4.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<h2>This is a heading</h2>

<p>This is a paragraph.</p>
<p>This is another paragraph.</p>
<button class="button">Click me</button>
</br></br>
<div id='readyDemo'></div>
<div id='readyDemo1'></div>
<div class='readyDemo2'></div>
<a href="https://google.ro" target = "_blank">Google blank</a>
<style>
	#readyDemo{
		font-family: Monotype Corsiva;
		font-size: 24px;
		color:green;
	}
	#readyDemo1{
		font-family:Arial;
		font-size: 16px;
		color:green;
	}
	.readyDemo2{
		font-family:Arial;
		font-size: 16px;
		color:green;
	}
</style>
<script>
$(document).ready(function(){
  $(".button").click(function(){
    $("p").hide();
  });
});
console.log(jQuery); //return a function
$.a = 'jQuerry';
console.log($); //return again a function - same
console.log(jQuery == $);//true
console.log('On line number 14');
$(document).ready(function() {
	console.log('Document is ready!!!');
});

$(document).ready(function() {
	//$('div')
	$('#readyDemo').text('Webpage is ready!');
});
$(document).ready(function() {
	//$('div')
	$('#readyDemo1').text('Edureka! jQuery').css('color', 'darkviolet').css('color', 'red').css('font-family', 'Times New Roman').css('font-size', 17).fadeIn("slow");
});
$(document).ready(function() {
	//$('div')
	$('.readyDemo2').text('Edureka! jQuery').css('color', 'darkviolet').css('color', 'darkpink').css('font-family', 'Times New Roman').css('font-size', 17).fadeIn("slow");

	$('h2').css('color', 'green').fadeIn("slow");

	/* $("a[target='_self']").css('text-decoration', 'none').css('color', 'blue');*/
	$("a").css('text-decoration', 'none').css('color', 'blue');

	$('#items>li').css('font-size',14).css('font-family','Arial');
	$('#items>li:first').css('text-decoration','underline');
	$('#items>li:last').css('font-size',20).css('font-family','Tahoma');

	$("button").click(function() {
		//$("#items").find("a").css('color','red');
		//$("#items").children("li").css('color','red');
		//$("#inside-div").closest("a").css('color','red'); 
		//$("a").parent().css('border','1px solid green');
		$("#items").find("a").css('color','red');
	});
});
	
</script>

<p>List 1:</p>
<ul id="items">
	<li id = "first">
		<a href="https://google.ro" target = "_blank">Outside</a>
		<div><a href="#inside" id="inside-div">Inside</a></div>
	</li>
	<li>Milk</li>
	<li>Tea</li>
</ul>
<button>Click me!</button>
</body>
</html>
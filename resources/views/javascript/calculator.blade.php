<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="/calculator/styles.css">
	<title>Calculator</title>
</head>
<body>
	<h1 class="center">My calculator</h1>

	<table>
		<tr>
			<td colspan = 4 class = 'calcul' id = 'result'>0</td>
			<!-- <td colspan = 4 class = 'calcul'><input type='textbox' id = 'result'></td> -->
		</tr>
		<tr>
			<td width='30px' id = 'clear'>AC</td>
			<td id = 'leftp'>(</td>
			<td id = 'rightp'>)</td>
			<td id = 'div'>/</td>
		</tr>
		<tr>
			<td id = 'num7'>7</td>
			<td id = 'num8'>8</td>
			<td id = 'num9'>9</td>
			<td id = 'mult'>*</td>
		</tr>
		<tr>
			<td id = 'num4'>4</td>
			<td id = 'num5'>5</td>
			<td id = 'num6'>6</td>
			<td id = 'sub'>-</td>
		</tr>
		<tr>
			<td id = 'num1'>1</td>
			<td id = 'num2'>2</td>
			<td id = 'num3'>3</td>
			<td id = 'plus'>+</td>
		</tr>
		<tr>
			<td colspan = 2 id = 'num0'>0</td>
			<td id = 'decimal'>.</td>
			<td id = 'equal'>=</td>
		</tr>
	</table>

	<script>
		function printSymbol(e)
		{
			let result = document.getElementById('result');
			//alert(e.target.id);
			let obj = e.target;
			result.textContent += obj.textContent;
		}
		// if clicked object is a digit
		function printDigit(e)
		{
			let result = document.getElementById('result');
			//alert(e.target.id);
			let obj = e.target;
			if(result.textContent == 0)
				result.textContent = obj.textContent;
			else
				result.textContent += obj.textContent;
		}
		// function printSymbol2()
		// {
		// 	let result = document.getElementById('result');
		// 	result.textContent = "2";
		// }
		 function clearScreen(e) //e is optional
		{
			document.getElementById('result').textContent = 0;
		}
		let result = document.getElementById('result');
		let num1 = document.getElementById('num1');
		let num2 = document.getElementById('num2');
		let num3 = document.getElementById('num3');
		let num4 = document.getElementById('num4');
		let num5 = document.getElementById('num5');
		let num6 = document.getElementById('num6');
		let num7 = document.getElementById('num7');
		let num8 = document.getElementById('num8');
		let num9 = document.getElementById('num9');
		let num0 = document.getElementById('num0');
		let plus = document.getElementById('plus');
		let sub = document.getElementById('sub');
		let mult = document.getElementById('mult');
		let div = document.getElementById('div');
		let decimal = document.getElementById('decimal');
		let clear = document.getElementById('clear');
		let equal = document.getElementById('equal');
		let rightp = document.getElementById('rightp');
		let leftp = document.getElementById('leftp');
		let nums = [num0, num1, num2, num3, num4,num5, num6, num7, num8, num9];

		for(let i = 0; i < nums.length; i++)
			//alert(i);
			nums[i].addEventListener('click', printDigit);

		let symbols = [plus, sub, mult, div, decimal, rightp, leftp];
		for(let j = 0; j < symbols.length; j++)
			symbols[j].addEventListener('click', printSymbol);

		//let symbols = document.getElementByClassName('symbol');

		// num1.addEventListener('click', printDigit);
		// num2.addEventListener('click', printDigit);
		// num3.addEventListener('click', printDigit);
		// num4.addEventListener('click', printDigit);
		// num5.addEventListener('click', printDigit);
		// num6.addEventListener('click', printDigit);
		// num7.addEventListener('click', printDigit);
		// num8.addEventListener('click', printDigit);
		// num9.addEventListener('click', printDigit);
		// num0.addEventListener('click', printDigit);
		// plus.addEventListener('click', printSymbol);
		// sub.addEventListener('click', printSymbol);
		// mult.addEventListener('click', printSymbol);
		// div.addEventListener('click', printSymbol);
		// decimal.addEventListener('click', printSymbol);
		// leftp.addEventListener('click', printSymbol);
		// rightp.addEventListener('click', printSymbol);

		
		// clear.addEventListener('click', 
		// 	e => document.getElementById('result').textContent = 0);
		//  () => document.getElementById('result').textContent = 0); // or arrow function
		clear.addEventListener('click', clearScreen);

		equal.addEventListener('click', 
			e => result.textContent = eval(result.textContent));

		// alert(num3.textContent);
		// num3.textContent = 'SUP';

		//----------------------------------------------
		// let a = [ 9, 7, 6, 4, 2];
		// let b = ['apple', 'lemon', 'peach']

		//alert(a[0]);
		//alert(b[0]);

		// for(let i = 0; i < b.length; i++)
		// 	alert(b[i]);

	</script>
</body>
</html>
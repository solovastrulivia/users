@extends('layouts.app')

@section('content')

<div class="container border border-info rounded  bg-light p-3 text-center">

	<h2>Examples using JavaScript</h2>
	<div class="row py-2 px-5">
			<a href="http://localhost/users/javascript/product" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Product</a>
			<a href="http://localhost/users/javascript/calc" class="btn btn-success btn-sm rounded btn-block px-5 py-2 m-1">Calculator</a>
			<a href="http://localhost/users/javascript/calculator" class="btn btn-primary btn-sm rounded btn-block px-5 py-2 m-1">Calculator 2</a>
	</div>	
</div>



@endsection

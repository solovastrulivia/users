<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/users/public/css/product.css">
	<link rel="stylesheet"  type="text/css" href="/users/public/css/styles.css">
	<title>Product</title>
</head>
<body>
	<h1 class= "center heading">Our products</h1>
		<br>
	<div class="center box">
			<table class="center">
				<tr class="left">
					<th>Name:</th>
					<td>toy</td>
				</tr>
				<tr class="left">
					<th>Description:</th>
					<td>little</td>
				</tr>
				<tr class="left">
					<th>Price:</th>
					<td><?php echo $price = 5; ?>$</td>
				</tr>
				<tr class="left">
					<th>Posting date:</th>
					<td><?php 
					$start_date = '20-01-2020';
					echo date('d m Y', strtotime($start_date)); ?></td>
				</tr>

				<tr class="left">
					<th>Posted for:</th>
					<td><?php
					$date1 = new DateTime('now');
    				$date2 = new DateTime($start_date);
    				$interval = $date1->diff($date2);
					 echo $interval->d;?> days ago</td>
				</tr>
				<tr class="left">
					<th>End promotion date:</th>
					<td><?php
					$last_date = '10-02-2020 20:30:00';
					echo date('d m Y', strtotime($last_date)); ?></td>
				</tr>
				<tr class="left">
					<th>Promotion for:</th>
					<td><?php
					$date1 = new DateTime('now');
    				$date2 = new DateTime($last_date);
    				$interval = $date2->diff($date1);
					 echo $interval->y . " years, " . $interval->m. " months, " .$interval->d. " days".$interval->h . " hours, ".$interval->i . " minutes, ".$interval->s . " seconds. ";?></td>
				</tr>
			</table>
	  
	  		<!-- <div class="center">
	  		<?php 
				$datetime1 = strtotime("now");
		  		$datetime2 = strtotime("$last_date");
				$interval  = abs($datetime2 - $datetime1);
				$minutes   = round($interval / 60);
				echo 'Diff. in minutes is: '.$minutes;
		  	?>
	  		</div>-->
	  		
	  	
		
	</div>
</body>
</html>
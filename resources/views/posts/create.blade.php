@extends('layouts.app')

@section('content')

@section('content_title')
Add a new member
@endsection

<div class="d-flex justify-content-center p-5">
	<div class="card bg-light" style="width: 60rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 pl-5">Add a member</h1>
		</div>
		<div class="card-body">
			 {{ Form::open( ['url' => route('posts.store') ] ) }}
        <div class="row form-group">
            <label class="col-3">{{ __('app.post_title') }}</label>
            <div class="col-6">
                {{ Form::text('title', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="row form-group">
            <label class="col-3">{{ __('app.content') }}</label>
            <div class="col-6">
                {{ Form::text('content', null, ['class' => 'form-control']) }}
            </div>
        </div>
        {{ Form::submit( __('app.save'), ['class' => 'btn btn-success'] )  }}
        {{ Form::close() }}
		</div>
	</div>
</div>

@endsection
<?php use Gregwar\Image\Image;?>

@extends('layouts.app')

@section('content')
<div class="container">
  <h2 class="with-border">{{ Lang::get('app.posts') }}
    <a href="{{ route('posts.create') }}" class="btn btn-info  m-1 pull-right">{{ __('app.new_post') }}</a>
</h2>

@if(count($posts)==0)
<div class="d-flex justify-content-center">No members in database!</div>
@else
<table class="container table table-striped table-hover border border-info  text-center py-4 mt-5">
  <tr class="bg bg-primary text-white">
    <th>{{ __('app.id') }}</th>
    <th>{{ __('app.title') }}</th>
    <th>{{ __('app.content') }}</th>
    <th>{{ __('app.created_at') }}</th>
    <th class="text-right">{{ __('app.actions') }}</th>
  </tr>
  @foreach($posts as $post)
  <tr>
    <td>{{$post->id}}</td>
    <td>{{$post->title}}</td>
    <td>{{$post->content}}</td>
    <td>{{$post->created_at}}</td>
    <td class="text-right">
      <a href="{{ route('posts.show', $post->id) }}" class="btn btn-secondary  m-1">{{ __('app.preview') }}</a>
      <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary  m-1">{{ __('app.edit') }}</a>
      {{ Form::open(['method'  => 'DELETE', 'url' => route('posts.delete', $post->id), 'class' => 'deleteForm  confirm-delete float-right mx-1 mt-0']) }}
          <button type="submit" class="btn btn-danger rounded m-1" onClick="return confirm( '{{ __('app.confirm.delete') }}' )">
              <i class="fa fa-trash"></i> {{ __('app.delete') }}
          </button>
      {{ Form::close()  }}
      <!-- a href="{ route('posts.delete', $post->id) }}" class="btn btn-danger  m-1" onClick="return confirm( '{ __('app.confirm.delete') }}' )">{ __('app.delete') }}</a> -->
    </td>
  </tr>
  @endforeach
</table>
@endif

<br/>
<div class="container d-flex justify-content-center">
  {!!Html::link("route('posts.create')",'New post',['class' => 'btn btn-info m-1'])!!}</div>
  <br/><br/>
  <div class="d-flex justify-content-center">
    @if(Session::has('message'))
    {{Session::get('message')}}
    @endif
  </div>
</div>
</div>
@endsection

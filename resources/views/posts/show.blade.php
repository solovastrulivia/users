@extends('layout')

@section('title')
Show member
@endsection

@section('header')
Cinema online
@endsection

@section('content')

@section('content_title')
Member details
@endsection
<br>
@section('nav1')
  <a class="nav-link active" href="/cinema-online/admin/members">Members</a>
@endsection
@section('nav2')
  <a class="nav-link" href="/cinema-online/admin/movies">Movies</a>
@endsection
@section('nav3')
  <a class="nav-link" href="/cinema-online/admin/movietheater">Movies theater</a>
@endsection
@section('nav4')
  <a class="nav-link" href="/cinema-online/admin/reviews">Reviews</a>
@endsection
<br>

<div class="jumbotron">
	<h1 class="p-2 text-primary">Details for: {{$member->name}}</h1>
	<br>
	<h3 class="p-2">Id: {{$member->id}}</h3>
	<h3 class="p-2">Age: {{$member->age}} years</h3>
	<h3 class="p-2">E-mail: {{$member->email}}</h3>
	<h3 class="p-2">Password: {{$member->password}}</h3>
	<h3 class="p-2">Confirm password: {{$member->confirm_password}}</h3>
	<h3 class="p-2">Type: {{$member->type}}</h3>
	<br>
	<span class="p-2">
		{!!Html::link("admin/members",'Back', ['class' => 'btn btn-primary'])!!}
	</span>
</div>

@endsection
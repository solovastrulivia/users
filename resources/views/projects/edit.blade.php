@extends('projects.layout')

@section('title','Edit a project')

@section('content')

<h1>Edit a project</h1>
<form method="post" action="/users/projects/{{ $project->id }}">
	{{ method_field('PATCH') }}
	@csrf

	<div>
		<input type="text" name="title" class="<?php echo $errors->has('title')?'error':'';?>" placeholder="Place a title" value="{{ $project->title }}">
	</div>
		<textarea class="<?php echo $errors->has('description')?'error':'';?>" name="description" placeholder="Place a description">{{$project->description}}</textarea>
	</div>
	<div>
		<button type="submit" class="m-1">Update</button>
	</div>	
</form>
	
	<form method="post" action="/users/projects/{{ $project->id }}">
		{{ method_field('DELETE') }}
		@csrf
		<div>
			<button type="submit" class="btn btn-danger m-1">Delete</button>
		</div>
	</form>
	@include('errors')
@endsection
@extends('projects.layout')

@section('title','Projects')

@section('content')
<h1 class="d-flex justify-content-left text-dark my-4">Projects</h1>
<ul class="list-group">
	@foreach($projects as $project)
	<li class="list-group-item m-1"> 		
		<div class="row">
			<div class="col-4">
				{{ $project->title }}
			</div>
			<div class="col-8">
				<a href="/users/projects/{{encrypt($project->id)}}/edit" class="btn btn-primary">Edit project</a>
				<a href="/users/projects/{{encrypt($project->id)}}" class="btn btn-success">Show project</a>
			</div>
		</div>
		</li>
@endforeach
</ul>

<a href="/users/projects/create" class="btn btn-danger m-2">Create a project</a>
@endsection
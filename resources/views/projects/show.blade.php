@extends('projects.layout')

@section('title','Project details')

@section('content')
<h1 class="d-flex justify-content-left text-dark my-4">{{$project->title}}</h1>

<h3 class="d-flex justify-content-left text-dark my-4">
	Description: {{$project->description}}</h3>
<h3 class="d-flex justify-content-left text-dark my-4">
	Last update: {{$project->updated_at}}</h3>
<h3 class="d-flex justify-content-left text-dark my-4">
	Created at: {{$project->created_at}}</h3>

@if($project->tasks->count())
<div>
	<h4>Tasks</h4>
<!-- PATCH /projects/id/tasks/id
	 PATCH /tasks/id -->
	@foreach($project->tasks as $task)
		<form method="POST" action="/users/completed-tasks/{{$task->id}}">
			@if($task->completed) 
				@method('DELETE')
			@endif
			
			<!-- @method('PATCH') -->
			@csrf
			<label class="checkbox {{ $task->completed?'is-complete':'' }}" for="completed">
				<input type="checkbox" name="completed" onChange="this.form.submit()" {{ $task->completed?'checked':'' }}>
				{{ $task->description }}
			</label>
		</form>
	@endforeach
	<br>
</div>
@endif

<!-- add a new task form -->

<div>
	<form method="post" action="/users/projects/{{$project->id}}/tasks">
		@csrf
		<label for="description"><b>New task</b></label><br>
		<input type="text" id="description" name="description" placeholder="New task">
		<button type="submit" name="submit">Add a task</button>
	</form>
</div>

	@include('errors')

<a href="/users/projects" class="btn btn-warning m-2">Back</a>
@endsection
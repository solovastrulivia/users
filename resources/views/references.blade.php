@extends('layouts.app')

@section('content')

<div class="container-fluid bg-light text-dark p-3">
	
	<h3 class="text-center m-2">References</h3>
	<ol class="px-5 py-3">
		<li><a href="https://www.tutorialspoint.com/laravel/index.htm" target="_blank">Laravel->tutorialspoint</a></li>
		<li><a href="https://laravel.com/docs/5.8"  target="_blank">Laravel 5.8 documentation</a></li>
		<li><a href="https://webdevetc.com/blog/laravel-naming-conventions"  target="_blank">Laravel naming conventions</a></li>
		<li><a href="https://code.tutsplus.com/tutorials/easy-form-validation-with-jquery--cms-33096"  target="_blank">jQuery validation</a></li>
		<li><a href="https://laravel.com/docs/5.7/encryption"  target="_blank">Laravel encryption to URL</a></li>
		<li><a href="
		https://dev.w3.org/html5/html-author/charref"  target="_blank">HTML symbols and entyties</a></li>
		<li><a href="
		https://laravel.com/docs/5.7/encryption"  target="_blank">Laravel encryption in URL</a></li>
		<li><a href="
		http://keith-wood.name/countdown.html?fbclid=IwAR2Xp1vZ4Co4lYrqvUZnKry5mS6ozoXhDomMOH4lDzSCC7TT_JcszeEFBxA"  target="_blank">jQuery time count down</a></li>
		<li><a href="
		https://dev.w3.org/html5/html-author/charref"  target="_blank">HTML symbols and entyties</a></li>
		<li><a href="
		https://medium.com/@chrissoemma/laravel-5-8-delete-and-soft-delete-practical-examples-b9b71c0a97f"  target="_blank">Laravel softDelete in URL</a></li>
		<li><a href="http://jsfiddle.net/d7kXX/"  target="_blank">Badges</a></li>
		<li><a href="https://laravel.com/docs/6.x/upgrade"  target="_blank">Uprade Laravel from 5.8 to 6.0</a></li>
		<li><a href="https://reinink.ca/articles/dynamic-relationships-in-laravel-using-subqueries"  target="_blank">Dynamic relationships using subqueries</a></li>
		<li><a href="https://laravel-news.com/running-make-auth-in-laravel-6"  target="_blank">Make auth in Laravel 6</a></li>
		<li><a href="https://dev.to/fadilxcoder/adding-multi-language-functionality-in-a-website-developed-in-laravel-4ech"  target="_blank">Adding multilanguage in website</a></li>
		<li><a href="https://www.itsolutionstuff.com/post/laravel-5-clear-cache-from-route-view-config-and-all-cache-data-from-applicationexample.html"  target="_blank">Laravel clear cache</a></li>
		<li><a href="https://medium.com/@sagarmaheshwary31/laravel-5-8-from-scratch-authentication-middleware-email-verify-and-password-reset-93a4b2103794"  target="_blank">Laravel Auth 5.8</a></li>
		<li><a href="#"  target="_blank"> *** Waiting for new reference</a></li>
		<li><a href="http://www.allitebooks.org/?s=laravel"  target="_blank">www.allitebooks.org</a></li>
		<li><a href="https://webdevetc.com/blog/laravel-naming-conventions"  target="_blank">Laravel Naming Conventions</a></li>
		<li><a href="https://morioh.com/p/0e306b75490b"  target="_blank">Laravel 7 - Auth region + bootstra4 link</a></li>
		<li><a href="https://laravel-news.com/laravel-5-4-key-too-long-error"  target="_blank">Laravel long key error</a></li>

		


</div>
<br><br><br>
<div class="container-fluid bg-light text-dark p-3">
	<h3 class="text-center m-2">Notes</h3>
	<ol class="px-5 py-3">
		<li>Virtual host</li>
		<p>
			C:\xampp\apache\conf\extra\httpd-vhosts.conf
			<br>
			&lt;VirtualHost &ast;:80 &gt;
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;DocumentRoot "C:/xampp/htdocs"
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;ServerName localhost
			<br>
			&lt;/VirtualHost&gt; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                         - pentru a vedea toate proiectele locale
			<br><br>
			C:\Windows\System32\drivers\etc\hosts&nbsp;&nbsp;  - deschis ca administrator
			<br>
			127.0.0.1 &nbsp;&nbsp;&nbsp;       localhost
			<br>
			127.0.0.1 &nbsp;&nbsp;&nbsp;       users.ro
			<br><br>
			XAMPP Control Panel: Stop si Start din nou
			<br><br>
			in URL: http://users.ro/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         - la mine domeniu securizat doar .ro	
		</p>
	</ol>
</div>

<!-- endsection -->
@stop
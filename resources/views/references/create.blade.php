@extends('layouts.app')

@section('content')

<div class="container p-5">
	<div class="row">
	  <label class="col-sm-4 col-form-label">{{ __('app.county') }}</label>
	  <div class="col-sm-8">
	    <div class="form-group{{ $errors->has('judet') ? ' has-danger' : '' }}">
	    {{ Form::select('judet', $judete, null, ['class' => "form-control " . ($errors->has('judet') ? 'is-invalid' : ''), 'placeholder' => __('app.county'), 'id' => 'input-judet', 'required' => 'true', 'aria-required' => 'true' ]) }}
	      @if ($errors->has('judet'))
	        <span id="name-error" class="error text-danger" for="input-judet">{{ $errors->first('judet') }}</span>
	      @endif
	    </div>
	  </div>
	</div>
	<div class="row">
	  <label class="col-sm-4 col-form-label">{{ __('app.city') }}</label>
	  <div class="col-sm-8">
	    <div class="form-group{{ $errors->has('localitate') ? ' has-danger' : '' }}">
	    {{ Form::select('localitate',  $localitati, null, ['class' => "form-control " . ($errors->has('localitate') ? 'is-invalid' : ''), 'placeholder' => __('app.city'), 'id' => 'input-localitate', 'required' => 'true', 'aria-required' => 'true' ]) }}
	      @if ($errors->has('localitate'))
	        <span id="name-error" class="error text-danger" for="input-localitate">{{ $errors->first('localitate') }}</span>
	      @endif
	    </div>
	  </div>
	</div>
</div>

@endsection

@section('footer-js')

    <script>
        $( function() {
            // console.log('test');
            // $("#input-judet").change(function(){
            // console.log('test2');
            //     $.ajax({
            //         url:        "{{ route('references.getOptionsList') }}",
            //         dataType:   'json',
            //         data:       'type=localitate&parent=' + $("#input-judet").val(),
            //         success:    function(response){
            //           console.log(response);
            //             $('#input-localitate').empty();
            //             $('#input-localitate').append('<option value=""> </option>');
            //             $.each(response, function (value, label){
            //                 $('#input-localitate').append('<option value="' + value + '">' + label + '</option>');
            //             })
            //         },
            //         error: function(xhr, ajaxOptions, thrownError){
            //             //alert('type=localitate&parent=' + $("#input-judet").val());
            //             alert(xhr.status);
            //         },
            //     })
            // })

            $("#input-judet").change(function(){
                if($('#input-judet').val()) {
                    console.log('val');
                    $.ajax({
                        url:        '{{ route('references.getOptionsList') }}?type=localitate&parent=' + $("#input-judet").val(),
                        dataType:   'json',
                        success:    function(response){
                            console.log(response);
                            $('#input-localitate').prop('disabled', false);
                            $('#input-localitate').empty();
                            $('#input-localitate').append('<option value=""> </option>');
                            $.each(response, function (value, label){
                                $('#input-localitate').append('<option value="' + value + '">' + label + '</option>');
                            })
                        },
                        error: function(xhr, ajaxOptions, thrownError){
                            alert('type=localitate&parent=' + $("#input-judet").val());
                            alert(xhr.status);
                        },
                    });
                } else {
                    $('#input-localitate').empty();
                    $('#input-localitate').prop('disabled', 'disabled');
          }
            });
        } );

    </script>
@endsection

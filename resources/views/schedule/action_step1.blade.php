@extends('layouts.app')


@section('content')

<div class="container bg-white">

	@foreach($disponibilities as $key => $disponibility)
	
		<div class="d-inline-block col-sm-4 float-left">
			<div class=" border border-dark w-3 m-3">
				@if(isset($indisponibil[$key])&&$indisponibil[$key]==true)
					<div class="text-dark text-left mt-2 mb-5 ml-3 fs18">
						{{$disponibility}}
					</div>
					<div class="d-flex justify-content-center text text-danger my-2 fs18">
						 Indisponibil
					</div>
				@else
					{{ Form::open(array('route' => 'schedule.storeStep1')) }}

					@csrf
					<div class="text-dark text-left mt-2 mb-5 ml-3 fs18">
						{{$disponibility}}
					</div>
					<div class="d-flex justify-content-center my-2">
						{{Form::hidden('id', $user_c->id)}}
						{{Form::hidden('date_in', $user_c->date_in)}}
						{{Form::hidden('disponibility', $disponibility)}}
						{{Form::submit('Programeaza', ['class' => 'btn buton-submit py-1 px-5'])}} 
					</div>					
					{{ Form::close()}}
				@endif
			</div>
		</div>		
	@endforeach
	<div class="clearfix"></div>
</div>


@endsection
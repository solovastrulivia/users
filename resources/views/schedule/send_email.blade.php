@extends('layouts.app')

@section('content')

<div class="container bg-white">
	<h2 class="text-center my-2">Multumimim pentru programare!</h2>
	<h4 class="text-center my-2">Pentru a fi contactat de catre doctor va rugam sa scrieti motivul programarii mai jos: </h4>
</div>
	{{ Form::open( ['url' => route('schedule.sendEmail') ] ) }}
	{{ Form::textarea('message', null, [ 'rows' => 5, 'cols' => 50, 'class' => 'form-control border rounded', 'style' => 'resize:none']) }}
	{{ Form::submit('Trimite') }}
	{{ Form::close() }}
	
<!-- endsection -->
@stop
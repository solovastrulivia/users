@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center my-4">{{ __('app.students') }}</h2>
        <div class="m-2">
            <a class="students btn btn-info offset-11 col-1 btn-add-students" id="btn-add-students">Elevi</a>
        </div>
        <div class="row">
            <div class="col-12" id="students">

            </div>
            
            <div class="col-12" id="note">
                
            </div>
        </div>
        {{-- 
            <table id="students" class="table table-striped">
            <thead>
                <tr>
                    <th>{{ __('app.name') }}</th>
                    <th class="text-right">{{ __('app.email') }}</th>
                    <th class="text-right">{{ __('app.age') }}</th>
                </tr>
            </thead>
            <tbody>
            @foreach($students as $student)
            <tr>
                <td>{{ $student->name }}<td>                 
                <td>{{ $student->email }}<td>
                <td>{{ $student->age }}<td>
            </tr>
            @endforeach
            </tbody>
        </table> 
        --}}
        
    </div>
@endsection

@section('footer-js')
<script>
    $(() => { 

        $('.btn-add-students').on('click', function(){
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: '{{ route("students.showTable") }}',
                success: function(result){
                    $('#students').html(result);
                    $('.btn-add-students').addClass('d-none');
                },
                error: function(xhr,status,error){
                    console.log(error);
                }
            });
        });

        $(document).on('click', '.btn-notes',function(){
            //alert($(this).data('id'));
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: '{{ route("students.showNotes") }}',
                data: 'id=' + $(this).data('id'),
                success: function(result){
                    $('#note').html(result);
                },
                error: function(xhr,status,error){
                    console.log(error);
                }
            });
        });

    });

   

</script>
@endsection

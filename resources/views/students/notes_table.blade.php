Notele studentului {{ $notes->first()->Student->name }} sunt:

<table class="table table-striped">
    <thead>
        <tr>
            <th>{{ __('app.materie') }}</th>
            <th>{{ __('app.nota') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($notes as $note)
        <tr>
            <td>{{ $note->Course->name }}</td>
            <td>{{ $note->notes }}</td>
        </tr>
        @endforeach
    </tbody>
</table> 
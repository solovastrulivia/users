<table id="students" class="table table-striped">
    <tr>
        <th width="25%">{{ __('app.name') }}</th>
        <th width="25%" class="text-right">{{ __('app.email') }}</th>
        <th width="25%" class="text-right">{{ __('app.age') }}</th>
        <th width="25%" class="text-right">{{ __('app.actiuni') }}</th>
    </tr>
    @foreach($students as $student)
        <tr>
            <td>{{ $student->name }}<td>                 
            <td>{{ $student->email }}<td>
            <td>{{ $student->age }}<td>
            <td>
                {{-- <a href="#" class="btn btn-success btn-notes">Note</a> --}}
                <a href="#" class="btn btn-success btn-notes" data-id="{{$student->id}}">{{ __('app.note') }}</a>
            <td>
        </tr>
    @endforeach
</table> 
@section('content')
    <div class="container">
        <h2 class="text-center my-4">{{ __('app.students') }}</h2>
        <div class="m-2">
            <a class="students btn btn-info offset-11 col-1 btn-add-students" id="btn-add-students">Elevi</a>
        </div>
        <div class="row">
            <div class="col-12" id="students">

            </div>
            
            <div class="col-12" id="note">
                
            </div>
        </div>       
    </div>
@endsection

@section('footer-js')
<script>
    $(() => { 

        $('.btn-add-students').on('click', function(){
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: '{{ route("students.showTable") }}',
                success: function(result){
                    $('#students').html(result);
                    $('.btn-add-students').addClass('d-none');
                },
                error: function(xhr,status,error){
                    console.log(error);
                }
            });
        });

    });
</script>
@endsection
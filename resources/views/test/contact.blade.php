@extends('test.layout')

@section('title', 'Contact')

@section('content')

	<h2>Contact page</h2>
	<div>
		<a href="/users/home">Home page</a>
		<a href="/users/contact">Contact page</a>
	</div>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime hic ab autem velit dolor a nesciunt. Quod quia tempore veniam, sunt aliquid necessitatibus eaque velit earum ratione provident, magnam harum.</p>
@endsection
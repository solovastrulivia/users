@extends('test.layout')

@section('title', 'Add pages')

@section('content')

<div class="d-flex justify-content-center py-4">
	<div class="card bg-light" style="width: 30rem;">
		<div class="card-title">
			<h1 class="text-primary p-3 pl-5">Add readen pages</h1>
		</div>
		<div class="card-body">
			{!!Form::open(array('action'=>'PagesController@store'))!!}
			@csrf

			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('user_id','User id:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('user_id', old('user_id'), ['class' => 'form-control', 'id' => 'user_id'])!!}
					{!!$errors->first('user_id')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('title','Title:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('title', old('title'), ['class' => "form-control input {{ $errors->has('title')?'is-danger':'' }}", 'id' => 'title'])!!}
					<!-- {!!$errors->first('title')!!} -->
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('author','Author:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('author', old('author'), ['class' => 'form-control', 'id' => 'author', 'required'])!!}
					{!!$errors->first('author')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('chapters','Chapters:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('chapters', old('chapters'), ['class' => 'form-control', 'id' => 'chapters', 'required'])!!}
					{!!$errors->first('chapters')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-4">
					{!!Form::label('no_pages_day','No of pages:')!!}
				</div>
				<div class="col-8">
					{!!Form::text('no_pages_day', old('no_pages_day'), ['class' => 'form-control', 'id' => 'no_pages_day', 'required'])!!}
					{!!$errors->first('no_pages_day')!!}
				</div>
			</div><br/>
			<div class="form-group row">
				<div class="col-12 d-flex justify-content-center">

					{{Form::hidden('id')}}
					{!!Form::submit('Add pages', ['class' => 'btn btn-primary'])!!}
				</div>
			</div>
			{!!Form::close()!!}
		</div>
		
		@if($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

@endsection


@extends('test.layout')

@section('title', 'Home')

@section('content')
	<h2>Home page</h2>
	<div>
		<a href="/users/home">Home page</a>
		<a href="/users/contact">Contact page</a>
		<a href="/users/welcome">Stuff</a>
		<a href="/users/myPage">My Page</a>
	</div>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime hic ab autem velit dolor a nesciunt. Quod quia tempore veniam, sunt aliquid necessitatibus eaque velit earum ratione provident, magnam harum.</p>

@endsection
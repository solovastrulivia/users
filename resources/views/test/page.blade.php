@extends('test.layout')

@section('title', 'Page')

@section('content')
<h1 class="d-flex justify-content-center py-4 text-primary">Welcome on my page!</h1>

<br>
<hr>
<br>

<h2 class="d-flex justify-content-center py-4 text-primary"> All pages from readen books</h2>
<div class='row'>
@foreach($pages as $page)
<div class="col-6">
	<div class="jumbotron p-4 m-3 border border-primary">
		<div class='row p-1'>
			<div class='col-4'>Title:</div>
	    	<div class='col-8'>{{$page->title}}</div>
		</div>
		<div class='row p-1'>
	    	<div class='col-4'>Author:</div>
	    	<div class='col-8'>{{$page->author}}</div>
	    </div>
	    <div class='row p-1'>
	    	<div class='col-4'>Chapters:</div>
	    	<div class='col-8'>{{$page->chapters}}</div>
	    </div>
	    <div class='row p-1'>
	    	<div class='col-4'>No of pages:</div>
	    	<div class='col-8'>{{$page->no_pages_day}}</div>
		</div>
	</div>
</div>
@endforeach
</div>

<div class="d-flex justify-content-center py-4">
	<a href="/users/pages/create" class="btn btn-primary"> Add readen pages</a>
</div>
@endsection


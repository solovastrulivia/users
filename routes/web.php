<?php

use App\Services\Twitter;
use App\Repositories\UserRepository;
use Illuminate\Filesystem\Filesystem;
use App\Post;
use App\User;
use App\Country;
use App\Photo;
use App\Tag;
use App\Address;


//dd(app(Filesystem::class));

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

Route::get('/', function (UserRepository $users) {
	//dd($users);
	//return view('welcome');
});

// app()->singleton('App\Services\Twitter', function() {
// 	return new \App\Services\Twitter('aaab'); 
// });

// app()->singleton('twitter', function() {
// 	return new \App\Services\Twitter('abcdef'); //config('services.twitter.api_key')
// });

// app()->singleton('App\Example', function() {
// 	dd('called');
// 	return new \App\Services\Example;
// });

// app()->bind('example', function() {
// 	return new \App\Example;
// });   //calling 2 times this willhave 2 diferent keys

// app()->singleton('example', function() {
// 	return new \App\Example;
// }); //calling 2 times this will have the same keys

Route::get('/', function (Twitter $twitter) {
	//dd($twitter);
	//dd(app('foo'));
	//dd(app('example'), app('example($e)'));
	//dd(app("App\Example"));
	//dd(app(Filesystem::class));  //error does not exist
	//factory(\App\User::class)->create();
	$user = \App\User::latest('id')->first();
	$roles = \App\Role::all();
	//$user->roles()->attach($roles);
	$user->roles()->sync([1, 2]);
	//dd($user,$roles);
	return view('first_page');
    //return view('welcome');
});

Route::resource('users', 'UsersController');
//Route::resource('roles', 'RolesController');
Route::resource('companies', 'CompaniesController');
Route::resource('products', 'ProductsController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', function () {
	
    return view('test.contact');
});

Route::get('/home', function () {
	
    return view('test.home');
});

Route::get('/references', function () {
	
    return view('references');
});

Route::get('/welcome', function () {

	$tasks = [
		'Go to the store',
		'Go to the market',
		'Go to work',
		'Go to the concert'
	];
	/*return view('test.welcome')->withTasks([
		'Go to the store',
		'Go to the market',
		'Go to work',
		'Go to the concert'
	]);*/
	//return view('test.welcome')->withTasks($tasks);
    return view('test.welcome', [
    	'tasks' => $tasks,
    	'title' => request('title'),
    	'foo' => 'Laracast'
    ]);
});


Auth::routes();

Route::resource('pages', 'PagesController');

Route::get('/myPage', 'PagesController@pages')/*->('myPages')->middleware('auth')*/;

/*
GET /projects (index)
GET /projects (create)
GET /projects/1 (show)
POST /projects (store)
GET /projects/1/edit (edit)
PATCH /projects/1 (update)
DELETE /projects/1 (destroy)
*/

//Route::resource('projects', 'ProjectsController')->middleware('can:');
Route::get('/projects', 'ProjectsController@index')/*->middleware('auth')*/;
Route::get('/projects/create', 'ProjectsController@create');
Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/{project}', 'ProjectsController@show');
Route::get('/projects/{project}/edit','ProjectsController@edit');
Route::patch('/projects/{project}', 'ProjectsController@update');
Route::delete('/projects/{project}', 'ProjectsController@destroy');

Route::post('/projects/{project}/tasks', 'ProjectTasksController@store');
Route::patch('/tasks/{task}', 'ProjectTasksController@update');

Route::post('/completed-tasks/{task}', 'CompletedTasksController@store');
Route::delete('/completed-tasks/{task}', 'CompletedTasksController@destroy');

//Auth::routes();
/**
 * Login Route(s)
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
/**
 * Register Route(s)
 */
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
/**
 * Password Reset Route(S)
 */
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/remember-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/remember-password', 'Auth\ResetPasswordController@reset')->name('password.update');
/**
 * Email Verification Route(s)
 */
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/admin', 'AdminController@index')->name('admin.dashboard');

Route::group(['prefix' => 'admin'], function () { 
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.loginForm');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login');
	Route::get('/register', 'Auth\AdminRegisterController@showRegistrationForm')->name('admin.registerForm');
Route::post('/register', 'Auth\AdminRegisterController@register')->name('admin.register');
});

//Route::group(['middleware' => 'auth', 'prefix' => 'setup'], function () { });

Route::group(['prefix' => 'user-calendar'], function () { 
	Route::get('/show', 'UserCalendarController@show')->name('userCalendar.show');
});

Route::group(['prefix' => 'schedule'], function () { 
	Route::get('/step1/{id}', 'ScheduleController@actionStep1')->name('schedule.actionStep1');
	Route::post('/store', 'ScheduleController@storeStep1')->name('schedule.storeStep1');
	Route::post('/send-email', 'ScheduleController@sendEmail')->name('schedule.sendEmail');
	Route::get('/email', 'ScheduleController@toEmail')->name('schedule.toEmail');
});

Route::group(['prefix' => 'ajax'], function () { 
	Route::get('/all-examples', 'AjaxController@allEx')->name('ajax.allEx');
	Route::get('/ex1', 'AjaxController@ajaxEx1')->name('ajax.ajaxEx1');
	Route::post('/ex1', 'AjaxController@ajaxEx1Post')->name('ajax.ajaxEx1Post');

	Route::get('/ex2', 'AjaxController@ajaxEx2')->name('ajax.ajaxEx2');
	Route::post('/ex2', 'AjaxController@ajaxEx2Post')->name('ajax.ajaxEx2Post');

	Route::get('/search-pages', 'AjaxController@searchPages')->name('ajax.searchPages');
	Route::get('/search-pages/action', 'AjaxController@searchPagesAction')->name('ajax.searchPagesAction');

	Route::get('/utilizare', 'AjaxController@showUtilizare')->name('ajax.showUtilizare');
	Route::get('/utilizare/action', 'AjaxController@showUtilizareAction')->name('ajax.showUtilizareAction');

	Route::resource('ajax-crud','AjaxCrudController');
});

    Route::resource('ajax-products','AjaxProductsController');
    Route::get('ajax-products','AjaxProductsController@index')->name('ajaxproducts.index');
    Route::post('ajax-products/store','AjaxProductsController@store')->name('ajaxproducts.store');
    //Route::delete('ajax-products/delete/{id}','AjaxProductsController@destroy')->name('ajaxproducts.delete');

Route::group(['prefix' => 'jQuery'], function () {
	Route::get('/all-examples', 'jQueryController@allEx')->name('jQuery.allEx');
	Route::get('/ex1', 'jQueryController@jQueryEx1')->name('jQuery.jQueryEx1');
	Route::get('/sole-first', 'jQueryController@showSole1')->name('jQuery.showSole1');
	Route::get('/sole-second', 'jQueryController@showSole2')->name('jQuery.showSole2');
	Route::get('/madison-market', 'jQueryController@showMarket')->name('jQuery.showMarket');
	Route::get('/intro-effects', 'jQueryController@introEffect')->name('jQuery.introEffect');
	Route::get('/first-image-hide', 'jQueryController@hideImage')->name('jQuery.hideImage');
	Route::get('/on-event', 'jQueryController@onEvent')->name('jQuery.onEvent');
	Route::get('/wschools', 'jQueryController@wschools')->name('jQuery.wschools');

	Route::get('/form', 'jQueryController@validateForm')->name('jQuery.validateForm');
	Route::post('/form', 'jQueryController@validateFormPost')->name('jQuery.validateFormPost');
});

Route::group(['prefix' => 'javascript'], function () {
	Route::get('/all-examples', 'JavaScriptController@allEx')->name('javascript.allEx');
	Route::get('/product', 'JavaScriptController@showProduct')->name('javascript.showProduct');
	Route::get('/calc', 'JavaScriptController@calc')->name('javascript.calc');
	Route::get('/calculator', 'JavaScriptController@calculator')->name('jQuery.calculator');
});

Route::get('/simple-modal', 'AjaxController@showModalPage')->name('modalPage');

Route::get('/index', 'AjaxController@index')->name('index');
Route::get('/modal', 'AjaxController@showModal')->name('modal');
Route::get('/create-modal', 'AjaxController@createModal')->name('createModal');
Route::post('/store-modal', 'AjaxController@storeModal')->name('storeModal');
Route::get('/edit-modal', 'AjaxController@editModal')->name('editModal');
Route::post('/update-modal/{id}', 'AjaxController@updateModal')->name('updateModal');
Route::delete('/delete/{id}', 'AjaxController@destroy')->name('deleteModal');

Route::group(['prefix' => 'blacklist'], function () {
	Route::get('/index', 'BlacklistController@index')->name('blacklist.index');
    Route::get('/dataTable', 'BlacklistController@dataTable')->name('blacklist.dataTable');
    Route::get('/dates', 'BlacklistController@showDates')->name('blacklist.showDates');
    Route::get('/dataTable2', 'BlacklistController@dataTable2')->name('blacklist.dataTable2');
});
//Route::get('/modal', 'AjaxController@showModal')->name('modal');


Route::group(['middleware' => ['web']], function () { 
	//Route - clasa, ::get - se apeleaza o metoda statica get
	//grupeaza (securizeaza - de la middleware) ce este interior intr-un fel de container 'web'
});

Route::get('/post/{id}/{name}', function($id, $name) {
	return "This is post number ". $id. " ".$name;
});

Route::get('admin/posts/example', array('as' => 'admin.home', function(){
	$url = route('admin.home');  //route - functie helper
	return "This url is ". $url; 
}));

//namespaces - ne lasa sa folosim functii, clase acelasi nume in diferite parti ale aceleasi aplicatii, sa nu apare coliziuni de nume la apelare

//php artisan route:list

// section  ->  endsection/stop
Route::group(['prefix' => 'ecourse'], function () {
	//Route::resource('EcourseController'); //gresit scris
	Route::get('/people', 'EcourseController@showPeople')->name('ecourse.people');
});

Route::group(['prefix' => 'posts'], function () {
	//Route::resource('members','PostsController');
	Route::get('/index', 'PostsController@index')->name('posts.index');
   	Route::get('/create', 'PostsController@create')->name('posts.create');
   	Route::post('/store', 'PostsController@store')->name('posts.store');
   	Route::get('/edit/{id}', 'PostsController@edit')->name('posts.edit');
   	Route::post('/update/{id}', 'PostsController@update')->name('posts.update');
   	Route::delete('/delete/{id}', 'PostsController@destroy')->name('posts.delete');
   	Route::get('/show/{id}', 'PostsController@show')->name('posts.show');
});

Route::get('/get-list', 'ReferencesController@getOptionsList')->name('references.getOptionsList');
Route::get('references/create', 'ReferencesController@create')->name('references.create');

Route::get('/posts/soft-delete', function(){

	Post::find(1)->delete();
	
});

//------------------------------------------------------------------------------------------------------------------
///Soft delete

Route::get('/posts/show-soft-delete', function(){

	//$posts = Post::withTrashed()->where('id', 1)->get();//postarea 1 care a fost stearsa
	$posts = Post::withTrashed()->get();//toate postarile si cele sterse
	$posts = Post::onlyTrashed()->get();//doar cele sterse
	return $posts;
});

Route::get('/posts/restore-soft-delete', function(){

	Post::withTrashed()->where('id', 1)->restore();
});

Route::get('/posts/force-delete', function(){

	Post::withTrashed()->where('id', 6)->forceDelete();
});

Route::get('/posts/force-delete', function(){

	Post::withTrashed()->where('id', 6)->forceDelete();
});

//--------------------------------------------------------------------------------
//Eloquent relationships
// //one to one
// Route::get('/user/{id}/post', function($id){

// 	return User::find($id)->post;
// 	//return User::find($id)->post->title;
// });
// //inverse
// Route::get('/post/{id}/user', function($id){

// 	//return Post::find($id)->user;
// 	//return Post::find($id)->user->name;
// });

//one to many
Route::get('/post/posts', function(){
	$user = User::find(2);
	//dd($user);
	foreach($user->posts as $post) {
		echo $post->title."</br>";
	}
});

//many to many

Route::get('/user/{id}/role', function($id){

	$users =  User::find($id)->roles()->orderBy('id', 'desc')->get();

	return $users;

	// foreach ($users as $role) {
	// 	echo $role->name. "<br>";
	// }
});

//Accessing the intermediate table -pivot

Route::get('user/pivot', function(){
	$user = User::find(2);
//dd($user->roles());
	foreach($user->roles as $role){
		echo $role->pivot->created_at."<br>";
	}
});

Route::get('user/country', function(){
	
	$country = Country::find(4);
	//dd($country, $country->posts);
	foreach($country->posts as $post){
		echo $post->title.'<br>';
	}

});


//Polimorphic relations


Route::get('user/photos', function() {
	$user = User::find(1);
	//dd($user, $user->photos);
	foreach($user->photos as $photo) {
		echo $photo.'<br>';
		//echo $photo->path.'<br>';
	}
});

Route::get('post/photos', function() {
	$post = Post::find(1);
	//dd($post, $post->photos);
	foreach($post->photos as $photo) {
		echo $photo.'<br>';
		//echo $photo->path.'<br>';
	}
});

// Route::get('post/{id}/photos', function($id) {
// 	$post = Post::find($id);
// 	//dd($post, $post->photos);
// 	foreach($post->photos as $photo) {
// 		echo $photo.'<br>';
// 		//echo $photo->path.'<br>';
// 	}
// });

//polymorphic inverse

Route::get('photo/{id}/post', function($id) {
	$photo = Photo::findOrFail($id);
	return $photo->imageable;
	//dd($post, $post->photos);
	foreach($post->photos as $photo) {
		echo $photo.'<br>';
		//echo $photo->path.'<br>';
	}
});

//Polymorphic many to many

Route::get('/post/tag', function() {
	$post = Post::find(1);

	foreach ($post->tags as $tag) {
		echo $tag->name.'<br>';
	}
});

Route::get('/tag/post', function() {
	$tag = Tag::find(1);

	foreach ($tag->posts as $post) {
		echo $post->title.'<br>';      //de revazut
	}
});

//Eloquent CRUD

// //one to one

Route::get('address/insert', function(){
	$user = User::findOrFail(2);
	//dd($user);
	$address = new Address(['name'=>'1234 Huston NY']);
	//dd($user->address());
	$user->address()->save($address);//poate insera in mai multe coloane  
		echo "done";
});

Route::get('address/update', function(){
	$address = Address::whereUserId(1)->first();
	//dd($address);
	$address->name = '1234 Huston';
	$address->save();
	echo "done";
});

Route::get('address/read', function(){
	$user = User::findOrFail(1);
	//dd($user);
	echo $user->address->name;
	echo '</br>'."done";
});

Route::get('address/delete', function(){
	$user = User::findOrFail(1);
	//dd($user);
	/*echo*/ $user->address->delete();  //return 1
	echo '</br>'."done";
});



//----------------------------------------------------------------------------------------------------------------------------------------------





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'students'], function () {
	//Route::resource('members','StudentsController');
   	// Route::get('/create', 'StudentsController@create')->name('students.create');
   	// Route::post('/store', 'StudentsController@store')->name('students.store');
   	// Route::get('/edit/{id}', 'StudentsController@edit')->name('students.edit');
   	// Route::post('/update/{id}', 'StudentsController@update')->name('students.update');
   	// Route::delete('/delete/{id}', 'StudentsController@destroy')->name('students.delete');
	// Route::get('/show/{id}', 'StudentsController@show')->name('students.show');
	Route::get('/table', 'StudentsController@showStudents')->name('students.showTable');
	Route::get('/notes-table', 'StudentsController@showNotes')->name('students.showNotes');

	Route::get('/', 'StudentsController@index')->name('students.index');
});

Route::group(['prefix' => 'books'], function () {
	Route::get('/', 'BooksController@index')->name('books.index');
	Route::post('/store', 'BooksController@storeBook')->name('books.storeBook');
});

